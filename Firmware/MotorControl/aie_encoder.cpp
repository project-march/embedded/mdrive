#include <odrive_main.h>
#include <math.h>
#include <bitset>
#include <crc.hpp>
#include <aie_encoder.hpp>

#define ERROR_VALUE 0

AieEncoder::AieEncoder(UART_HandleTypeDef* huart) :
    huart_(huart)
{
}

bool AieEncoder::apply_config() {
    config_.parent = this;

    return true;
}

void AieEncoder::setup() {
    mode_ = config_.mode;
}

void AieEncoder::do_checks() {
    if(config_.enable_aie_encoder) {
        if(axis_->requested_state_ == 0) startup_done = true;

        if((startup_done && (axis_->axis_num_ == 1) && (pos_abs_ == ERROR_VALUE))) {
            axis_->encoder_.set_error(Encoder::ERROR_UNCONNECTED_AIE_CABLE, false);
        }
    }
}

void AieEncoder::update() {
    if(config_.enable_aie_encoder) {
        switch (mode_)
        {
            case MODE_MARCH: {
                int32_t cpr = config_.cpr_aie; 
                uint32_t zero_position = config_.zero_position_aie;

                pos_abs_rad = config_.aie_orientation*((float)pos_abs_ - (float)zero_position)*(2*M_PI/(float)cpr);
                pos_abs_rad_ = pos_abs_rad;
            } break;
            
            default: {
                axis_->encoder_.set_error(Encoder::ERROR_UNSUPPORTED_AIE_ENCODER_MODE);
            } break;
        }
    }
}