#ifndef __AIE_ENCODER_HPP
#define __AIE_ENCODER_HPP

class AieEncoder;

#include <board.h> // needed for arm_math.h
#include <Drivers/STM32/stm32_spi_arbiter.hpp>
#include "utils.hpp"
#include <autogen/interfaces.hpp>
#include "component.hpp"

class AieEncoder : public ODriveIntf::AieEncoderIntf {
    public:
        struct Config_t {
            Mode mode = MODE_MARCH;
            int32_t cpr_aie = 16384;                // Overwritten by mdrivepython
            int32_t aie_orientation = 1;            // Overwritten by mdrivepython
            int32_t min_pos_abs_aie = 9;            // Dummy value, overwritten by mdrivepython
            int32_t max_pos_abs_aie = 9;            // Dummy value, overwritten by mdrivepython
            int32_t zero_position_aie = 9;          // Dummy value, overwritten by mdrivepython

            bool enable_aie_encoder = false;

            AieEncoder* parent = nullptr;
        };

    Config_t config_;

    AieEncoder(UART_HandleTypeDef* huart);

    bool apply_config();
    void setup();
    void do_checks();
    void update();

    UART_HandleTypeDef* huart_;
    Axis* axis_ = nullptr; // set by Axis constructor

    uint32_t pos_abs_ = 0;
    uint8_t status_ = 0;
    bool startup_done = false;

    OutputPort<float> pos_abs_rad_ = 0.0f;

    float pos_abs_rad = 0.0f;
    Mode mode_;
};

#endif // __AIE_ENCODER_HPP