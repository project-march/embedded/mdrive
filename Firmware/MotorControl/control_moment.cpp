
#include "odrive_main.h"
#include <algorithm>
#include <string>
#include <iostream>

float MomentControlloop::update(Controller* controller_) {

    //Temporary input/output port substitution
    controller_->moment_estimate_ = controller_->axis_->torque_sensor_.converted_moment;
    controller_->dummy_value_ = *controller_->moment_estimate_;

    // float moment;
    
    if (controller_->config_.control_mode == ODriveIntf::ControllerIntf::ControlMode::CONTROL_MODE_MOMENT_CONTROL ||
        controller_->config_.control_mode == ODriveIntf::ControllerIntf::ControlMode::CONTROL_MODE_FUZZY_CONTROL){


        if(!controller_->config_.velocity_limiting){
            // velocity limited torque control (prevents the velocity to drive away)
            // Set error
            // float vel_compensation;
            
            moment_err = controller_->moment_setpoint_ - *controller_->moment_estimate_;

            // vel_compensation = (moment_err/controller_->vel_setpoint_)* std::move(*controller_->vel_estimate); 
            // moment_err_comp = moment_err - vel_compensation;

            // Implement P-action
            moment_p += controller_->config_.moment_gain * moment_err;

            // Implement D-action
            moment_d = ((moment_err - pre_moment_err)/current_meas_period) * controller_->config_.moment_deriv_gain; // normally use comp

            // Calculate output
            controller_->moment_ = moment_p + moment_d;

            // Save error to previous error
            pre_moment_err = moment_err; 
            pre_moment_err_comp = moment_err_comp;
       }

    }

    float torque = controller_->torque_setpoint_;

    // Anti-cogging is enabled after calibration
    // We get the current position and apply a current feed-forward
    // ensuring that we handle negative encoder positions properly (-1 == motor->encoder.encoder_cpr - 1)
    if (controller_->anticogging_valid_ && controller_->config_.anticogging.anticogging_enabled) {
        if (!controller_->anticogging_pos_estimate.has_value()) {
            controller_->set_error(ODriveIntf::ControllerIntf::Error::ERROR_INVALID_ESTIMATE);
            return false;
        }
        float anticogging_pos = *controller_->anticogging_pos_estimate / controller_->axis_->encoder_.getCoggingRatio();
        torque += controller_->config_.anticogging.cogging_map[std::clamp(mod((int)anticogging_pos, 3600), 0, 3600)];
    }

    if (controller_->config_.control_mode == ODriveIntf::ControllerIntf::ControlMode::CONTROL_MODE_MOMENT_CONTROL ||
        controller_->config_.control_mode == ODriveIntf::ControllerIntf::ControlMode::CONTROL_MODE_FUZZY_CONTROL ){
        torque += controller_->moment_ * controller_->moment_to_torque_ * controller_->axis_->motor_.config_.kv / controller_->axis_->encoder_.config_.transmission;
    }    

    // Torque limiting
    // bool limited = false;
    float Tlim = controller_->axis_->motor_.max_available_torque();
    if (torque > Tlim) {
        // limited = true;
        torque = Tlim;
    }
    if (torque < -Tlim) {
        // limited = true;
        torque = -Tlim;
    }


    // power calculation for spinout check
    float ideal_electrical_power = 0.0f;
    if (controller_->axis_->motor_.config_.motor_type != Motor::MOTOR_TYPE_GIMBAL) {
        ideal_electrical_power = controller_->axis_->motor_.current_control_.power_ - \
            SQ(controller_->axis_->motor_.current_control_.Iq_measured_) * 1.5f * controller_->axis_->motor_.config_.phase_resistance - \
            SQ(controller_->axis_->motor_.current_control_.Id_measured_) * 1.5f * controller_->axis_->motor_.config_.phase_resistance;
    }
    else {
        ideal_electrical_power = controller_->axis_->motor_.current_control_.power_;
    }
    controller_->mechanical_power_ += controller_->config_.mechanical_power_bandwidth * current_meas_period * (torque * *controller_->vel_estimate * M_PI * 2.0f - controller_->mechanical_power_);
    controller_->electrical_power_ += controller_->config_.electrical_power_bandwidth * current_meas_period * (ideal_electrical_power - controller_->electrical_power_);

    // Spinout check
    // If mechanical power is negative (braking) and measured power is positive, something is wrong
    // This indicates that the controller is trying to stop, but torque is being produced.
    // Usually caused by an incorrect encoder offset
    if (controller_->mechanical_power_ < controller_->config_.spinout_mechanical_power_threshold && controller_->electrical_power_ > controller_->config_.spinout_electrical_power_threshold) {
        controller_->set_error(ODriveIntf::ControllerIntf::Error::ERROR_SPINOUT_DETECTED);
        return false;
    }

    return torque;
}
