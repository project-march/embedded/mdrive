#ifndef __CONTROL_MOMENT_HPP
#define __CONTROL_MOMENT_HPP

class Controller;

class MomentControlloop{
private:
    float moment_err = 0.0f;
    float moment_err_comp = 0.0f; 
    float moment_p = 0.0f;
    float moment_d = 0.0f;
public:    
    float update(Controller* controller_);
    // float moment_to_torque = 0.12092;
    float pre_moment_err = 0.0f;
    float pre_moment_err_comp = 0.0f;
};

#endif // __CONTROL_MOMENT_HPP
