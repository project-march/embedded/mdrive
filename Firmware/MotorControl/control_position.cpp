
#include "odrive_main.h"
#include <algorithm>
#include <string>
#include <iostream>

float PositionControlloop::update(Controller* controller_) {

    //Temporary input/output port substitution
    controller_->pos_abs_rad = controller_->axis_->encoder_.pos_abs_rad;
    controller_->dummy_pos_ = *controller_->pos_abs_rad;
    controller_->vel_rad = controller_->axis_->encoder_.vel_rad;
    controller_->dummy_vel_rad_ = *controller_->vel_rad;

    float pos_gain = controller_->config_.pos_gain;
    float int_gain = controller_->config_.integral_gain;
    float vel_gain = controller_->config_.vel_gain;
    float vel_integrator_gain = controller_->config_.vel_integrator_gain;

    if (controller_->config_.gain_scheduling) {

        float delta_P = controller_->config_.delta_P_gain_scheduling*2.0*current_meas_period;
        float delta_I = controller_->config_.delta_I_gain_scheduling*2.0*current_meas_period;
        float delta_D = controller_->config_.delta_D_gain_scheduling*2.0*current_meas_period;
        
        controller_->check_ += current_meas_period;

        if (controller_->config_.pos_gain > controller_->last_P_gain_) {
            controller_->new_P_gain_ = controller_->last_P_gain_+ delta_P;
        }
        if (controller_->config_.integral_gain < controller_->last_I_gain_) {
            controller_->new_I_gain_ = controller_->last_I_gain_ + delta_I;
        }
        if (controller_->config_.vel_gain < controller_->last_D_gain_) {
            controller_->new_D_gain_ = controller_->last_D_gain_ + delta_D;
        }
        if (controller_->config_.pos_gain < controller_->last_P_gain_) {
            controller_->new_P_gain_ = controller_->last_P_gain_- delta_P;
        }
        if (controller_->config_.integral_gain > controller_->last_I_gain_) {
            controller_->new_I_gain_ = controller_->last_I_gain_ - delta_I;
        }
        if (controller_->config_.vel_gain > controller_->last_D_gain_) {
            controller_->new_D_gain_ = controller_->last_D_gain_ - delta_D;
        }

        //set the gains
        float pos_gain = controller_->new_P_gain_;
        float int_gain = controller_->new_I_gain_;
        float vel_gain = controller_->new_D_gain_;
    } 

    float gain_scheduling_multiplier = 1.0f;
    float vel_des = controller_->vel_setpoint_;
    if (controller_->config_.control_mode == ODriveIntf::ControllerIntf::ControlMode::CONTROL_MODE_POSITION_CONTROL ||
        controller_->config_.control_mode == ODriveIntf::ControllerIntf::ControlMode::CONTROL_MODE_FUZZY_CONTROL) {
        float pos_err;

        if (controller_->config_.circular_setpoints) {
            if (!controller_->pos_estimate_circular.has_value() || !controller_->pos_wrap.has_value()) {
                controller_->set_error(ODriveIntf::ControllerIntf::Error::ERROR_INVALID_ESTIMATE);
                return false;
            }
            // Keep pos setpoint from drifting
            controller_->pos_setpoint_ = fmodf_pos(controller_->pos_setpoint_, *controller_->pos_wrap);
            // Circular delta
            pos_err = controller_->pos_setpoint_ - *controller_->pos_estimate_circular;
            pos_err = wrap_pm(pos_err, *controller_->pos_wrap);
        } else {
            if (!controller_->pos_estimate_linear.has_value()) {
                controller_->set_error(ODriveIntf::ControllerIntf::Error::ERROR_INVALID_ESTIMATE);
                return false;
            }
            pos_err = controller_->pos_setpoint_ - *controller_->pos_abs_rad;
            // pos_err = pos_setpoint_ - *pos_estimate_linear;
        }

        // reset controller

        // P-term position control
        float position_p; 
        position_p += pos_gain * pos_err;  

        // I-term position control (necessary for observed steady state error)
        float position_i;

        controller_->prev_pos_err_ = pos_err;



        if (pos_err > controller_->config_.max_delta_pos && controller_->axis_->current_state_ == Axis::AXIS_STATE_CLOSED_LOOP_CONTROL){
            controller_->set_error(ODriveIntf::ControllerIntf::Error::ERROR_STEP_TOO_LARGE);
            return false;
        }

        controller_->direction_check_ = controller_->prev_pos_err_*(controller_->prev_pos_abs_rad_-*controller_->pos_abs_rad);

        // int i = 0;
        // if (controller_->axis_->current_state_ == Axis::AXIS_STATE_CLOSED_LOOP_CONTROL){
        //     if ((controller_->direction_check_>1e-5)){
                
        //             controller_->set_error(ODriveIntf::ControllerIntf::Error::ERROR_WRONG_DIRECTION);
        //             return false;
        //     }
        // }
        // controller_->prev_pos_abs_rad_ = *controller_->pos_abs_rad;
        

        if (pos_err*controller_->prev_pos_err_ >= 0.0f){
            controller_->integral_ += pos_err * current_meas_period; 
            controller_->integral_ = std::clamp(controller_->integral_, -0.1f, 0.1f); 
        } else {
            controller_->integral_ = 0.0f;
        }

        position_i = controller_->integral_ * int_gain; 

        // Combine to PI-controller
        vel_des += position_p + position_i;


        // V-shaped gain shedule based on position error
        float abs_pos_err = std::abs(pos_err);
        if (controller_->config_.enable_gain_scheduling && abs_pos_err <= controller_->config_.gain_scheduling_width) {
            gain_scheduling_multiplier = abs_pos_err / controller_->config_.gain_scheduling_width;
        }
    }

    // Velocity limiting
    float vel_lim = controller_->config_.vel_limit;
    if (controller_->config_.enable_vel_limit) {
        vel_des = std::clamp(vel_des, -vel_lim, vel_lim);
    }

    controller_->vel_des_limited_ = vel_des;

    // Check for overspeed fault (done in this module (controller) for cohesion with vel_lim)
    if (controller_->config_.enable_overspeed_error) {  // 0.0f to disable
        if (!controller_->vel_estimate.has_value()) {
            controller_->set_error(ODriveIntf::ControllerIntf::Error::ERROR_INVALID_ESTIMATE);
            return false;
        }
        if (std::abs(*controller_->vel_estimate) > controller_->config_.vel_limit_tolerance * vel_lim) {
            controller_->set_error(ODriveIntf::ControllerIntf::Error::ERROR_OVERSPEED);
            return false;
        }
    }

    // TODO Change to controller working in torque units
    // Torque per amp gain scheduling (ACIM)
    if (controller_->axis_->motor_.config_.motor_type == Motor::MOTOR_TYPE_ACIM) {
        float effective_flux = controller_->axis_->acim_estimator_.rotor_flux_;
        float minflux = controller_->axis_->motor_.config_.acim_gain_min_flux;
        if (std::abs(effective_flux) < minflux)
            effective_flux = std::copysignf(minflux, effective_flux);
        vel_gain /= effective_flux;
        vel_integrator_gain /= effective_flux;
        // TODO also scale the integral value which is also changing units.
        // (or again just do control in torque units)
    }

    // Velocity control
    float torque = controller_->torque_setpoint_;

    // Anti-cogging is enabled after calibration
    // We get the current position and apply a current feed-forward
    // ensuring that we handle negative encoder positions properly (-1 == motor->encoder.encoder_cpr - 1)
    if (controller_->anticogging_valid_ && controller_->config_.anticogging.anticogging_enabled) {
        if (!controller_->anticogging_pos_estimate.has_value()) {
            controller_->set_error(ODriveIntf::ControllerIntf::Error::ERROR_INVALID_ESTIMATE);
            return false;
        }
        float anticogging_pos = *controller_->anticogging_pos_estimate / controller_->axis_->encoder_.getCoggingRatio();
        torque += controller_->config_.anticogging.cogging_map[std::clamp(mod((int)anticogging_pos, 3600), 0, 3600)];
    }

    float v_err = 0.0f;
    if (controller_->config_.control_mode >= ODriveIntf::ControllerIntf::ControlMode::CONTROL_MODE_VELOCITY_CONTROL) {
        if (!controller_->vel_estimate.has_value()) {
            controller_->set_error(ODriveIntf::ControllerIntf::Error::ERROR_INVALID_ESTIMATE);
            return false;
        }

        v_err = vel_des - *controller_->vel_rad;
        torque += (vel_gain * gain_scheduling_multiplier) * v_err;

        // Velocity integral action before limiting
        torque += controller_->vel_integrator_torque_;
    }

    // Velocity limiting in current mode
    if (controller_->config_.control_mode < ODriveIntf::ControllerIntf::ControlMode::CONTROL_MODE_VELOCITY_CONTROL && controller_->config_.enable_current_mode_vel_limit) {
        if (!controller_->vel_estimate.has_value()) {
            controller_->set_error(ODriveIntf::ControllerIntf::Error::ERROR_INVALID_ESTIMATE);
            return false;
        }
        torque = controller_->limitVel(controller_->config_.vel_limit, *controller_->vel_estimate, vel_gain, torque);
    }

    // Torque limiting
    bool limited = false;
    float Tlim = controller_->axis_->motor_.max_available_torque();
    if (torque > Tlim) {
        limited = true;
        torque = Tlim;
    }
    if (torque < -Tlim) {
        limited = true;
        torque = -Tlim;
    }

    // Velocity integrator (behaviour dependent on limiting)
    if (controller_->config_.control_mode < ODriveIntf::ControllerIntf::ControlMode::CONTROL_MODE_VELOCITY_CONTROL) {
        // reset integral if not in use
        controller_->vel_integrator_torque_ = 0.0f;
    } else {
        if (limited) {
            // TODO make decayfactor configurable
            controller_->vel_integrator_torque_ *= 0.99f;
        } else {
            controller_->vel_integrator_torque_ += ((vel_integrator_gain * gain_scheduling_multiplier) * current_meas_period) * v_err;
        }
    }

    float ideal_electrical_power = 0.0f;
    if (controller_->axis_->motor_.config_.motor_type != Motor::MOTOR_TYPE_GIMBAL) {
        ideal_electrical_power = controller_->axis_->motor_.current_control_.power_ - \
            SQ(controller_->axis_->motor_.current_control_.Iq_measured_) * 1.5f * controller_->axis_->motor_.config_.phase_resistance - \
            SQ(controller_->axis_->motor_.current_control_.Id_measured_) * 1.5f * controller_->axis_->motor_.config_.phase_resistance;
    }
    else {
        ideal_electrical_power = controller_->axis_->motor_.current_control_.power_;
    }
    controller_->mechanical_power_ += controller_->config_.mechanical_power_bandwidth * current_meas_period * (torque * *controller_->vel_estimate * M_PI * 2.0f - controller_->mechanical_power_);
    controller_->electrical_power_ += controller_->config_.electrical_power_bandwidth * current_meas_period * (ideal_electrical_power - controller_->electrical_power_);

    // Spinout check
    // If mechanical power is negative (braking) and measured power is positive, something is wrong
    // This indicates that the controller is trying to stop, but torque is being produced.
    // Usually caused by an incorrect encoder offset
    if (controller_->mechanical_power_ < controller_->config_.spinout_mechanical_power_threshold && controller_->electrical_power_ > controller_->config_.spinout_electrical_power_threshold) {
        controller_->set_error(ODriveIntf::ControllerIntf::Error::ERROR_SPINOUT_DETECTED);
        return false;
    }

    controller_->last_P_gain_ = controller_->new_P_gain_;
    controller_->last_I_gain_ = controller_->new_I_gain_;
    controller_->last_D_gain_ = controller_->new_D_gain_;

    controller_->prev_torque_ = torque;

    return torque;
}