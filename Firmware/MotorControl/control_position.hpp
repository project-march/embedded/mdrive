#ifndef __CONTROL_POSITION_HPP
#define __CONTROL_POSITION_HPP

class Controller;

class PositionControlloop{
private:
    float position_p = 0.0f; 
public:
    float update(Controller* controller_);
};

#endif // __CONTROL_POSITION_HPP
