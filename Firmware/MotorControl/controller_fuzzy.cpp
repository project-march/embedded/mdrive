
#include "odrive_main.h"
#include <algorithm>
#include <string>
#include <iostream>
#include <stdexcept>
#include <cstdlib> 
#include <ctime>
// #define TORQUEDEBUG
#include <complex>


bool Controller::apply_config() {
    config_.parent = this;
    update_filter_gains();
    return true;
}

void Controller::reset() {
    pos_setpoint_ = 0.0f;
    vel_setpoint_ = 0.0f;
    vel_integrator_torque_ = 0.0f;
    torque_setpoint_ = 0.0f;
    mechanical_power_ = 0.0f;
    electrical_power_ = 0.0f;
    moment_setpoint_ = 0.0f;
}

void Controller::set_error(Error error) {
    error_ |= error;
    last_error_time_ = odrv.n_evt_control_loop_ * current_meas_period;
}

//--------------------------------
// Command Handling
//--------------------------------


void Controller::move_to_pos(float goal_point) {
    axis_->trap_traj_.planTrapezoidal(goal_point, pos_setpoint_, vel_setpoint_,
                                 axis_->trap_traj_.config_.vel_limit,
                                 axis_->trap_traj_.config_.accel_limit,
                                 axis_->trap_traj_.config_.decel_limit);
    axis_->trap_traj_.t_ = 0.0f;
    trajectory_done_ = false;
}

void Controller::move_incremental(float displacement, bool from_input_pos = true){
    if(from_input_pos){
        input_pos_ += displacement;
    } else{
        input_pos_ = pos_setpoint_ + displacement;
    }

    input_pos_updated();
}

void Controller::start_anticogging_calibration() {
    // Ensure the cogging map was correctly allocated earlier and that the motor is capable of calibrating
    if (axis_->error_ == Axis::ERROR_NONE) {
        config_.anticogging.calib_anticogging = true;
    }
}

/*
 * This anti-cogging implementation iterates through each encoder position,
 * waits for zero velocity & position error,
 * then samples the current required to maintain that position.
 * 
 * This holding current is added as a feedforward term in the control loop.
 */
bool Controller::anticogging_calibration(float pos_estimate, float vel_estimate) {
    float pos_err = input_pos_ - pos_estimate;
    if (std::abs(pos_err) <= config_.anticogging.calib_pos_threshold / (float)axis_->encoder_.config_.cpr &&
        std::abs(vel_estimate) < config_.anticogging.calib_vel_threshold / (float)axis_->encoder_.config_.cpr) {
        config_.anticogging.cogging_map[std::clamp<uint32_t>(config_.anticogging.index++, 0, 3600)] = vel_integrator_torque_;
    }
    if (config_.anticogging.index < 3600) {
        config_.control_mode = CONTROL_MODE_FUZZY_CONTROL;
        input_pos_ = config_.anticogging.index * axis_->encoder_.getCoggingRatio();
        input_vel_ = 0.0f;
        input_torque_ = 0.0f;
        input_moment_ = 0.0f;
        input_pos_updated();
        input_moment_updated();
        input_moment_ = 0.0f;
        return false;
    } else {
        config_.anticogging.index = 0;
        config_.control_mode = CONTROL_MODE_FUZZY_CONTROL;
        input_pos_ = 0.0f;  // Send the motor home
        input_vel_ = 0.0f;
        input_torque_ = 0.0f;
        input_moment_ = 0.0f;
        input_pos_updated();
        input_moment_updated();
        input_moment_ = 0.0f;
        anticogging_valid_ = true;
        config_.anticogging.calib_anticogging = false;
        return true;
    }
}

void Controller::update_filter_gains() {
    float bandwidth = std::min(config_.input_filter_bandwidth, 0.25f * current_meas_hz);
    input_filter_ki_ = 2.0f * bandwidth;  // basic conversion to discrete time
    input_filter_kp_ = 0.25f * (input_filter_ki_ * input_filter_ki_); // Critically damped
}

float Controller::limitVel(const float vel_limit, const float vel_estimate, const float vel_gain, const float torque) {
    float Tmax = (vel_limit - vel_estimate) * vel_gain;
    float Tmin = (-vel_limit - vel_estimate) * vel_gain;
    return std::clamp(torque, Tmin, Tmax);
}

static int xorShiftSeed = 0xDEADBEEF;

int Controller::xorshift() {
    xorShiftSeed ^= (xorShiftSeed << 13);
    xorShiftSeed ^= (xorShiftSeed >> 17);
    xorShiftSeed ^= (xorShiftSeed << 5);
    return xorShiftSeed;
}

void Controller::create_butterworth_lowpass(float cutoff_freq, std::vector<float>&a, std::vector<float>&b) {
    float Ts = current_meas_period;
    int order = 2;
    float samplerate = 1.0f / current_meas_period;
    float wc = 2 * samplerate * tan(PI * cutoff_freq / samplerate);
    wc_ = wc;

    std::vector<std::complex<double>> poles(order);
    for (int k = 0; k < order; ++k) {
        double theta = PI * (2.0 * k + 1.0) / (2.0 * order);
        poles[k] = std::polar(1.0, theta);
    }

    // Transform poles to discrete time
    std::vector<std::complex<double>> poles_dt(order);
    for (int k = 0; k < order; ++k) {
        poles_dt[k] = (2.0 + wc) / (2.0 - wc) + poles[k] * (2.0 - wc) / (2.0 + wc);
    }

    // Compute b and a
    a.resize(order + 1);
    b.resize(order + 1);

    a[0] = 1.0;  
    for (int i = 1; i <= order; ++i) {
        a[i] = -poles_dt[i-1].real();
    }

    double gain = 1.0;
    for (int i = 0; i < order; ++i) {
        gain *= 2.0 * samplerate - wc * poles[i].real();
    }
    b[0] = gain;
    for (int i = 1; i <= order; ++i) {
        b[i] = 0.0;
    }

    float frequency_ratio = cutoff_freq / samplerate;
    float ita = 1.0 / tan(PI * frequency_ratio);
    float q = sqrt(2.0);

    b[0] = (1.0 / (1.0 + ita / q + ita * ita));
    b[1] = 2.0 * b[0];
    b[2] = b[0];
    // a[0] = 0.1;
    a[1] = 2.0 * (ita * ita - 1.0) * b[0];
    a[2] = -(1.0 - ita / q + ita * ita) * b[0];
    check_buffer_factors(a, b);
}

float factorial(int num) {
    int fact = 1;
    for (int i = 1; i <= num; ++i) {
        fact *= i;
    }
    return (float)fact;
}

// Function to calculate permutations P(n, k)
float binomial_coefficient(int n, int k) {
    return factorial(n) / (factorial(n - k) * factorial(k));
}

void Controller::create_thiran_lowpass(float cutoff_freq, std::vector<float>&a, std::vector<float>&b) {
    float Ts = current_meas_period;
    int order = config_.order_;
    float samplerate = 1.0f / current_meas_period;

    float time_delay = 1.0/cutoff_freq;

    float D = time_delay /(2.0* current_meas_period);
    

    std::vector<float> coefficients;
    coefficients.resize(order + 1);

    coefficients[0] = 1.0;
    float large_pi = 1.0;
    for (int k = 1; k <= order; ++k) {
        for (int i = 0; i <= order; ++i) {
            large_pi *= (D-order+i)/(D-order+i+k);
        }
        coefficients[k] = binomial_coefficient(order, k) * large_pi* std::pow(-1.0, k);
        large_pi = 1.0;
    }

    a.resize(order + 1);
    b.resize(order + 1);

    // coefficients[0] = 1.0;
    // coefficients[1] = -2.0*(D-2.0)/(D+1.0);
    // coefficients[2] = (D-1.0)*(D-2.0)/((D+1.0)*(D+2.0));

    b[0] = coefficients[2];
    b[1] = coefficients[1];
    b[2] = coefficients[0];
    a[0] = coefficients[0];
    a[1] = -1.0*coefficients[1];
    a[2] = -1.0*coefficients[2];

    check_buffer_factors(a, b);
}

void Controller::check_buffer_factors(std::vector<float> a, std::vector<float> b){
    check_a_0_ = a[0];  
    check_a_1_ = a[1];
    check_a_2_ = a[2];
    check_b_0_ = b[0];
    check_b_1_ = b[1];
    check_b_2_ = b[2];
}

void Controller::check_buffer_input_output(std::vector<float>&inputHistory, std::vector<float>&outputHistory) {
    check_input_buffer_0_ = inputHistory[0];
    check_input_buffer_1_ = inputHistory[1];
    check_input_buffer_2_ = inputHistory[2];
    check_output_buffer_0_ = outputHistory[0];
    check_output_buffer_1_ = outputHistory[1];
    check_output_buffer_2_ = outputHistory[2];

}

bool Controller::update() {
    std::optional<float> anticogging_pos_estimate = axis_->encoder_.pos_estimate_.present();
    std::optional<float> anticogging_vel_estimate = axis_->encoder_.vel_estimate_.present();
    std::optional<float> anticogging_moment_estimate = axis_->torque_sensor_.moment_estimate_.present();
    float pos_abs = axis_->encoder_.pos_abs_rad;


    if (axis_->step_dir_active_) {
        if (!pos_wrap.has_value()) {
            set_error(ERROR_INVALID_CIRCULAR_RANGE);
            return false;
        }
        input_pos_ = (float)(axis_->steps_ % config_.steps_per_circular_range) * (*pos_wrap / (float)(config_.steps_per_circular_range));
    }

    if (config_.anticogging.calib_anticogging) {
        if (!anticogging_pos_estimate.has_value() || !anticogging_vel_estimate.has_value() || !anticogging_moment_estimate.has_value()) {
            set_error(ERROR_INVALID_ESTIMATE);
            return false;
        }
        // non-blocking
        anticogging_calibration(*anticogging_pos_estimate, *anticogging_vel_estimate);
    }

    // TODO also enable circular deltas for 2nd order filter, etc.
    if (config_.circular_setpoints) {
        if (!pos_wrap.has_value()) {
            set_error(ERROR_INVALID_CIRCULAR_RANGE);
            return false;
        }
        input_pos_ = fmodf_pos(input_pos_, *pos_wrap);
    }




    // Update inputs
    switch (config_.input_mode) {
        case INPUT_MODE_PASSTHROUGH: {
            pos_setpoint_ = input_pos_;
            vel_setpoint_ = input_vel_;
            torque_setpoint_ = input_torque_; 
            moment_setpoint_ = input_moment_;
            noise_ = 0.0;
            last_input_mode_ = INPUT_MODE_PASSTHROUGH;
        } break;
        case INPUT_MODE_TUNING: {
            autotuning_phase_ = wrap_pm_pi(autotuning_phase_ + (2.0f * M_PI * autotuning_.frequency * current_meas_period));
            float c = our_arm_cos_f32(autotuning_phase_);
            float s = our_arm_sin_f32(autotuning_phase_);

            // if (skip_tuning_ = 160) {
                pos_setpoint_ =  input_pos_ +  autotuning_.pos_amplitude * s; // + pos_amp_c * c
            //     skip_tuning_ = 0;
            // } else {
            //     skip_tuning_++;
            // }

            double noise_int = static_cast<double>(xorshift());

            noise_int_ = noise_int;

            float noise = autotuning_.noise_amp*(-1.0f + 2.0f * noise_int) /xor_limit_;
              // Generate a single random floating-point number between -1.0 and 1.0
            noise_ = noise;

            // vel_setpoint_ = input_vel_ + autotuning_.vel_amplitude * c;
            torque_setpoint_ = noise;

            last_input_mode_ = INPUT_MODE_TUNING;
        } break;
        case INPUT_MODE_POS_FILTER: {
            // 2nd order pos tracking filter
            float delta_pos = input_pos_ - pos_setpoint_; // Pos error
            if (config_.circular_setpoints) {
                if (!pos_wrap.has_value()) {
                    set_error(ERROR_INVALID_CIRCULAR_RANGE);
                    return false;
                }
                delta_pos = wrap_pm(delta_pos, *pos_wrap);
            }
            float delta_vel = input_vel_ - vel_setpoint_; // Vel error
            float accel = input_filter_kp_*delta_pos + input_filter_ki_*delta_vel; // Feedback
            torque_setpoint_ = accel * config_.inertia; // Accel
            vel_setpoint_ += current_meas_period * accel; // delta vel
            pos_setpoint_ += current_meas_period * vel_setpoint_; // Delta pos
        } break;
        case INPUT_MODE_BUTTERWORTH_FILTER: {
            std::vector<float> a;
            std::vector<float> b;   
            create_butterworth_lowpass(config_.input_filter_bandwidth, a, b);
            // create_thiran_lowpass(config_.input_filter_bandwidth, a, b);
            std::vector<float> inputHistory;
            
            // check_ = binomial_coefficient(2, 1);

            prev_prev_input_ = prev_input_;
            prev_input_ = input_pos_;

            output_filter_ = 0.0f;
            
            inputHistory = {input_pos_,prev_input_,prev_prev_input_};

            if (last_input_mode_ != config_.input_mode ) {
                itt = 0;
            }
            
            if (itt<=1) {
                // Not enough history to apply the filter
                itt++;
                pos_setpoint_ = pos_abs;
            } else if (itt<=2){
                for (size_t i = 0; i < b.size()-1; ++i) {
                    output_filter_ += b[i] * inputHistory[i];
                    output_filter_b_ += b[i] * inputHistory[i];
                }
                for (size_t i = 1; i < a.size()-1; ++i) {
                    output_filter_ += a[i] * outputHistory[i];
                    output_filter_a_ += a[i] * outputHistory[i];
                }

                pos_setpoint_ = pos_abs;
                itt++;
            } else {
                for (size_t i = 0; i < b.size(); ++i) {
                    output_filter_ += b[i] * inputHistory[i];
                    output_filter_b_ += b[i] * inputHistory[i];
                }
                for (size_t i = 1; i < a.size(); ++i) {
                    output_filter_ += a[i] * outputHistory[i];
                    output_filter_a_ += a[i] * outputHistory[i];
                }

                pos_setpoint_ = output_filter_;
            }

            

            // Update the output filter history
            prev_prev_output_ = prev_output_;
            prev_output_ = output_filter_;

            outputHistory = {output_filter_, prev_output_, prev_prev_output_};

            check_buffer_input_output(inputHistory, outputHistory);

            last_input_mode_ = INPUT_MODE_BUTTERWORTH_FILTER;
        } break;
        case INPUT_MODE_EXPONENTIAL_MOVING_AVERAGE: {
            // Exponential moving average
            float alpha = 2.0f * M_PI * config_.input_filter_bandwidth * current_meas_period;
            pos_setpoint_ = alpha * input_pos_ + (1.0f - alpha) * pos_setpoint_;
        } break;
        default: {
            // TODO: apply more valid error (such as: MODE_NOT_IMPLEMENTED)
            set_error(ERROR_INVALID_INPUT_MODE);
            return false;
        }
    }    
    // Get output current from position and torque control
    result_moment_loop_ = moment_controlloop_->update(this);
    result_position_loop_ = position_controlloop_->update(this);

    // check to make sure that we don't take the one input if other control mode
    if (config_.control_mode == CONTROL_MODE_POSITION_CONTROL){
        // weight_moment_ = 0.0f;
        // weight_position_ = 1.0f;
    }
    if (config_.control_mode == CONTROL_MODE_MOMENT_CONTROL){
        // weight_position_ = 0.0f;
        // weight_moment_ = 1.0f;
    }

    float surplus = std::abs(weight_moment_ + weight_position_ - 1);

    if (surplus > std::numeric_limits<float>::epsilon()){
        weight_moment_ = weight_moment_ / (1+surplus);
        weight_position_ = weight_position_ / (1+surplus);
    }

    resulting_current_ = result_moment_loop_ * weight_moment_ + result_position_loop_ * weight_position_;
    #ifdef TORQUEDEBUG
    // To not apply any current:
    torque_output_ = 0.0f;
    return true;
    #endif
    torque_output_ = resulting_current_;

    return true;
}
