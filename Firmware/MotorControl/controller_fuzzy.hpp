#ifndef __CONTROLLER_FUZZY_HPP
#define __CONTROLLER_FUZZY_HPP

class PositionControlloop;
class MomentControlloop;

class Controller : public ODriveIntf::ControllerIntf {
public:
    struct Anticogging_t {
        uint32_t index = 0;
        float cogging_map[3600];
        bool pre_calibrated = false;
        bool calib_anticogging = false;
        float calib_pos_threshold = 1.0f;
        float calib_vel_threshold = 1.0f;
        float cogging_ratio = 1.0f;
        bool anticogging_enabled = true;
    };

    struct Autotuning_t {
        float frequency = 0.0f;
        float pos_amplitude = 0.0f;
        float pos_phase = 0.0f;
        float vel_amplitude = 0.0f;
        float vel_phase = 0.0f;
        float torque_amplitude = 0.0f;
        float torque_phase = 0.0f;
        float noise_amp = 0.0f;
    };

    struct Config_t {
        ControlMode control_mode = CONTROL_MODE_FUZZY_CONTROL; // Overwritten by ODrivepython  //see: ControlMode_t
        InputMode input_mode = INPUT_MODE_PASSTHROUGH;             //see: InputMode_t
        float pos_gain = 20.0f;                  // [(turn/s) / turn]
        float integral_gain = 0.0f;              // reset controller
        float vel_gain = 1.0f / 6.0f;            // [Nm/(turn/s)]
        // float vel_gain = 0.2f / 200.0f,       // [Nm/(rad/s)] <sensorless example>
        float vel_integrator_gain = 0.0f; // Overwritten by ODrivepython, [Nm/(turn/s * s)]
        float vel_limit = 2.0f; // Overwritten by Odrivepython                  // [turn/s] Infinity to disable.
        float vel_limit_tolerance = 1.2f;        // ratio to vel_lim. Infinity to disable.
        float vel_ramp_rate = 1.0f;              // [(turn/s) / s]
        float torque_ramp_rate = 0.01f;          // Nm / sec
        float max_delta_pos = 0.5f;              // [turns] maximum delta pos before error
        bool circular_setpoints = false;
        float circular_setpoint_range = 1.0f;    // Circular range when circular_setpoints is true. [turn]
        uint32_t steps_per_circular_range = 1024;
        float inertia = 0.0f;                    // [Nm/(turn/s^2)]
        float input_filter_bandwidth = 125.0f;     // [Hz]
        int order_ = 2;
        float homing_speed = 0.25f;              // [turn/s]
        Anticogging_t anticogging;
        float gain_scheduling_width = 10.0f;
        bool enable_gain_scheduling = false;
        bool enable_vel_limit = true;
        bool enable_overspeed_error = true; // Overwritten by Odrivepython
        bool enable_current_mode_vel_limit = true; // Overwritten by Odrivepython  // enable velocity limit in current control mode (requires a valid velocity estimator)
        uint8_t axis_to_mirror = -1;
        float mirror_ratio = 1.0f;
        float torque_mirror_ratio = 0.0f;
        uint8_t load_encoder_axis = -1;  // default depends on Axis number and is set in load_configuration(). Set to -1 to select sensorless estimator.
        float mechanical_power_bandwidth = 20.0f; // [rad/s] filter cutoff for mechanical power for spinout detction
        float electrical_power_bandwidth = 20.0f; // [rad/s] filter cutoff for electrical power for spinout detection
        float spinout_electrical_power_threshold = 10.0f; // Overwritten by Odrivepython // [W] electrical power threshold for spinout detection
        float spinout_mechanical_power_threshold = -10.0f; // Overwritten by Odrivepython // [W] mechanical power threshold for spinout detection

        float moment_gain = 1.0f;
        float moment_deriv_gain = 0.001f;

        bool velocity_limiting = false;

        bool gain_scheduling = false;
        float delta_P_gain_scheduling = 1.0f;
        float delta_I_gain_scheduling = 1.0f;
        float delta_D_gain_scheduling = 1.0f;

        // custom setters
        Controller* parent;
        void set_input_filter_bandwidth(float value) { input_filter_bandwidth = value; parent->update_filter_gains(); }
        void set_steps_per_circular_range(uint32_t value) { steps_per_circular_range = value > 0 ? value : steps_per_circular_range; }
    };

    Controller() {};
    
    bool apply_config();

    void reset();
    void set_error(Error error);

    constexpr void input_pos_updated() {
        input_pos_updated_ = true;
    }

    constexpr void input_moment_updated() {
        input_moment_updated_ = true;
    }

    bool select_encoder(size_t encoder_num);

    // Trajectory-Planned control
    void move_to_pos(float goal_point);
    void move_incremental(float displacement, bool from_goal_point);
    
    // TODO: make this more similar to other calibration loops
    void start_anticogging_calibration();
    bool anticogging_calibration(float pos_estimate, float vel_estimate);

    void update_filter_gains();
    bool update();

    float limitVel(const float vel_limit, const float vel_estimate, const float vel_gain, const float torque);

    Config_t config_;
    Axis* axis_ = nullptr; // set by Axis constructor
    MomentControlloop* moment_controlloop_;
    PositionControlloop* position_controlloop_;

    Error error_ = ERROR_NONE;
    float last_error_time_ = 0.0f;  

    // Inputs
    InputPort<float> pos_estimate_linear_src_;
    InputPort<float> pos_estimate_circular_src_;
    InputPort<float> vel_estimate_src_;
    InputPort<float> pos_wrap_src_;
    InputPort<float> pos_abs_rad_src_;
    InputPort<float> vel_rad_src_;
    InputPort<float> moment_estimate_src_;

    std::optional<float> pos_estimate_linear = pos_estimate_linear_src_.present();
    std::optional<float> pos_estimate_circular = pos_estimate_circular_src_.present();
    std::optional<float> pos_wrap = pos_wrap_src_.present();
    std::optional<float> vel_estimate = vel_estimate_src_.present();
    std::optional<float> pos_abs_rad = pos_abs_rad_src_.present();
    std::optional<float> vel_rad = vel_rad_src_.present();
    std::optional<float> moment_estimate_ = moment_estimate_src_.present();

    std::optional<float> anticogging_pos_estimate;
    std::optional<float> anticogging_vel_estimate;
    

    float pos_setpoint_ = 0.0f; // [turns]
    float vel_setpoint_ = 0.0f; // [turn/s]
    // float vel_setpoint = 800.0f; <sensorless example>
    float vel_integrator_torque_ = 0.0f;    // [Nm]
    float torque_setpoint_ = 0.0f;  // [Nm]
    float moment_setpoint_ = 0.0f;

    float input_pos_ = 0.0f;     // [turns]  // Only overwritten by ODrivepython with actuation test
    float input_vel_ = 0.0f;     // [turn/s] // Only overwritten by ODrivepython with actuation test
    float input_torque_ = 0.0f;  // [Nm] // Only overwritten by ODrivepython with actuation test

    float integral_ = 0.0f;     // reset controller
    float prev_pos_err_ = 0.0f;  // reset controller
    float vel_des_limited_ = 0.0f;
    float prev_torque_ = 0.0f;
    float prev_pos_abs_rad_ = 0.0f;
    float direction_check_ = 0.0f;
    float noise_ = 0.0f;
    float noise_int_ = 0.0f;
    float xor_limit_ = 4294967296.0f;
    float output_filter_ = 0.0f;
    float prev_input_ = 0.0f;
    float prev_prev_input_ = 0.0f;
    float prev_output_ = 0.0f;
    float prev_prev_output_ = 0.0f;
    int itt = 0;
    float check_ = 0.0f;
    float check_a_0_ = 0.0f;
    float check_a_1_ = 0.0f;
    float check_a_2_ = 0.0f;
    float check_b_0_ = 0.0f;
    float check_b_1_ = 0.0f;
    float check_b_2_ = 0.0f;
    float check_input_buffer_0_ = 0.0f;
    float check_input_buffer_1_ = 0.0f;
    float check_input_buffer_2_ = 0.0f;
    float check_output_buffer_0_ = 0.0f;
    float check_output_buffer_1_ = 0.0f;
    float check_output_buffer_2_ = 0.0f;
    uint32_t check_size_a_ = 0;
    uint32_t check_size_b_ = 0;
    float output_filter_a_ = 0.0f;
    float output_filter_b_ = 0.0f;
    float wc_ = 0.0f;
    std::vector<float> outputHistory = {0.0f, 0.0f, 0.0f};
    InputMode last_input_mode_ = INPUT_MODE_INACTIVE;

    float last_P_gain_ = 0.0f;
    float last_I_gain_ = 0.0f;
    float last_D_gain_ = 0.0f;

    float new_P_gain_ = 0.0f;
    float new_I_gain_ = 0.0f;
    float new_D_gain_ = 0.0f;
    
    float input_filter_kp_ = 0.0f;
    float input_filter_ki_ = 0.0f;
    float input_moment_ = 0.0f;

    Autotuning_t autotuning_;
    float autotuning_phase_ = 0.0f;
    
    bool input_pos_updated_ = false;
    bool input_moment_updated_ = false; 

    float weight_moment_ = 0.0f;
    float weight_position_ = 1.0f;

    float moment_;
    float resulting_current_;
    float result_moment_loop_;
    float result_position_loop_;
    float dummy_pos_;
    float dummy_value_;
    float dummy_vel_rad_;
    float moment_to_torque_ = 0.12092f;
    int32_t skip_tuning_ = 0;

    float extra_dummy_ = 0.0f;

    int xorshift();
    void create_butterworth_lowpass(float cutoff_freq, std::vector<float>&a, std::vector<float>&b);
    void create_thiran_lowpass(float cutoff_freq, std::vector<float>&a, std::vector<float>&b);
    void check_buffer_factors(std::vector<float>a, std::vector<float>b);
    void check_buffer_input_output(std::vector<float>&a, std::vector<float>&b);
   

    bool trajectory_done_ = true;

    bool anticogging_valid_ = false;
    float mechanical_power_ = 0.0f; // [W]
    float electrical_power_ = 0.0f; // [W]

    // Outputs
    OutputPort<float> torque_output_ = 0.0f;

    // custom setters
    void set_input_pos(float value) { input_pos_ = value; input_pos_updated(); }
    void set_input_moment(float value) {input_moment_ = value; input_moment_updated(); }
    void set_weight_position(float value) {weight_position_ = value;}
    void set_weight_moment(float value) {weight_moment_ = value;}
};

#endif // __CONTROLLER_HPP
