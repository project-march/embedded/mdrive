#ifndef __ENCODER_HPP
#define __ENCODER_HPP

class Encoder;

#include <board.h> // needed for arm_math.h
#include <Drivers/STM32/stm32_spi_arbiter.hpp>
#include "utils.hpp"
#include <string>
#include "component.hpp"
#include <bitset>

class Encoder : public ODriveIntf::EncoderIntf {
public:
    struct Config_t {
        Mode mode = MODE_MARCH; // Overwritten by Odrivepython
        float calib_range = 0.02f; // Overwritten by Odrivepython
        float calib_scan_distance = 20.0f * M_PI; // Overwritten by Odrivepython
        float calib_scan_omega = 4.0f * M_PI;
        float bandwidth = 1000.0f;
        int32_t phase_offset = 0;
        float phase_offset_float = 0.0f;
        int32_t cpr = 4096; // Overwritten by Odrivepython
        float index_offset = 0.0f;
        bool use_index = false; // Overwritten by Odrivepython
        bool pre_calibrated = true; // Overwritten by Odrivepython
        int32_t direction = 0;
        bool use_index_offset = true;
        bool enable_phase_interpolation = true;
        bool find_idx_on_lockin_only = false;
        bool ignore_illegal_hall_state = false;
        uint8_t hall_polarity = 0;
        bool hall_polarity_calibrated = false;
        uint16_t sincos_gpio_pin_sin = 2;
        uint16_t sincos_gpio_pin_cos = 3;

        // custom setters
        Encoder* parent = nullptr;
        void set_use_index(bool value) { use_index = value; parent->set_idx_subscribe(); }
        void set_find_idx_on_lockin_only(bool value) { find_idx_on_lockin_only = value; parent->set_idx_subscribe(); }
        void set_pre_calibrated(bool value) { pre_calibrated = value; parent->check_pre_calibrated(); }
        void set_bandwidth(float value) { bandwidth = value; parent->update_pll_gains(); }

        int32_t enc_orientation = 0;
        int32_t inc_enc_orientation = -1;
        int32_t lin_inc_enc_orientation = -1;
        int32_t min_pos_abs = 373620;
        int32_t max_pos_abs = 674255;
        float transmission = 101;

        int32_t cpr_incremental = 4096; // Overwritten by Odrivepython
        int32_t cpr_absolute = 1048576; // Overwritten by Odrivepython
        int32_t transmission_cpr = cpr_incremental * transmission;
        int32_t zero_position = 416640; // Overwritten by Odrivepython
        float zero_position_angle = 0.0f;
    };

    Config_t config_;

    Encoder(TIM_HandleTypeDef* timer, Stm32Gpio index_gpio,
            Stm32Gpio hallA_gpio, Stm32Gpio hallB_gpio, Stm32Gpio hallC_gpio);

    // uint8_t get_pos_abs(uint8_t pos_abs_init, uint8_t shadow_count);
    bool apply_config(ODriveIntf::MotorIntf::MotorType motor_type);
    void setup();
    void set_error(Error error, bool isFatal = true);
    bool do_checks();

    void enc_index_cb();
    void set_idx_subscribe(bool override_enable = false);
    void update_pll_gains();
    void check_pre_calibrated();

    void set_linear_count(int32_t count);
    void set_circular_count(int32_t count, bool update_offset);

    bool run_index_search();
    bool run_direction_find();
    bool run_offset_calibration();
    void sample_now();
    bool update();

    void update_delta_enc(int32_t delta_enc); // Added MIX

    TIM_HandleTypeDef* timer_;
    Stm32Gpio index_gpio_;
    Stm32SpiArbiter* spi_arbiter_;
    UART_HandleTypeDef* huart_;
    Axis* axis_ = nullptr; // set by Axis constructor

    Error error_ = ERROR_NONE;
    bool index_found_ = false;
    bool is_ready_ = false;
    int32_t shadow_count_ = 0;
    int32_t count_in_cpr_ = 0;
    int32_t delta_enc_ = 0;
    int64_t encvaluesum_ = 0;
    int32_t num_steps_ = 0;
    float interpolation_ = 0.0f;
    OutputPort<float> phase_ = 0.0f;     // [rad]
    OutputPort<float> phase_vel_ = 0.0f; // [rad/s]
    float pos_estimate_counts_ = 0.0f;  // [count]
    float pos_cpr_counts_ = 0.0f;  // [count]
    float delta_pos_cpr_counts_ = 0.0f;  // [count] phase detector result for debug
    float vel_estimate_counts_ = 0.0f;  // [count/s]
    float pll_kp_ = 0.0f;   // [count/s / count]
    float pll_ki_ = 0.0f;   // [(count/s^2) / count]
    float calib_scan_response_ = 0.0f; // debug report from offset calib
    int32_t pos_abs_pre_crc_ = 0;
    uint16_t test_value_ = 0;
    uint32_t pos_abs_ = 0;
    uint32_t pos_abs_ethercat_ = 0;
    uint8_t orbis_status_ = 0;
    float spi_error_rate_ = 0.0f;
    float calib_range_reached_ = 0.0f;
    float expected_encoder_delta_ = 1.0f;
    float elec_rad_per_enc_ = 1.0f;

    OutputPort<float> pos_estimate_ = 0.0f; // [turn]
    OutputPort<float> vel_estimate_ = 0.0f; // [turn/s]
    OutputPort<float> pos_circular_ = 0.0f; // [turn]
    OutputPort<float> pos_abs_rad_ = 0.0f;
    OutputPort<float> vel_rad_ = 0.0f;

    bool startup_done = false;
    bool pos_abs_updated_ = false;
    float pos_abs_rad = 0.0f;
    float pos_abs_rad_actual_ = 0.0f;
    float vel_rad = 0.0f;

    bool pos_estimate_valid_ = false;
    bool vel_estimate_valid_ = false;

    int16_t tim_cnt_sample_ = 0; // 
    uint8_t hall_state_ = 0x0; // bit[0] = HallA, .., bit[2] = HallC
    std::optional<uint8_t> last_hall_cnt_ = std::nullopt; // Used to find hall edges for calibration
    bool calibrate_hall_phase_ = false;
    bool sample_hall_states_ = false;
    bool sample_hall_phase_ = false;
    std::array<int, 8> states_seen_count_; // for hall polarity calibration
    std::array<int, 6> hall_phase_calib_seen_count_;

    float sincos_sample_s_ = 0.0f;
    float sincos_sample_c_ = 0.0f;

    Mode mode_;

    constexpr float getCoggingRatio() { return 1.0f / 3600.0f;}
};

#endif // __ENCODER_HPP
