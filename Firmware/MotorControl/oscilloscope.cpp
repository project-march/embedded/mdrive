
#include "oscilloscope.hpp"

// if you use the oscilloscope feature you can bump up this value
#define OSCILLOSCOPE_SIZE 4096

void Oscilloscope::update() {
    float trigger_data = trigger_src_ ? *trigger_src_ : 0.0f;
    float trigger_threshold = trigger_threshold_;
    float sample_data = data_src_ ? **data_src_ : 0.0f;
    float sample_data2 = data_src2_ ? **data_src2_ : 0.0f;

    oscilloscope_type sample_data_short = float_to_half(sample_data);
    oscilloscope_type sample_data_short2 = float_to_half(sample_data2);

    if (trigger_data < trigger_threshold) {
        ready_ = true;
    }
    if (ready_ && trigger_data >= trigger_threshold) {
        capturing_ = true;
        ready_ = false;
    }
    if (capturing_) {
        if (pos_ < OSCILLOSCOPE_SIZE) {
            data_[pos_++][0] = sample_data_short;
            data_[pos_][1] = sample_data_short2;
        } else {
            pos_ = 0;
            capturing_ = false;
        }
    }
}
