#ifndef __OSCILLOSCOPE_HPP
#define __OSCILLOSCOPE_HPP

#include <autogen/interfaces.hpp>

// if you use the oscilloscope feature you can bump up this value
#define OSCILLOSCOPE_SIZE 4096

class Oscilloscope : public ODriveIntf::OscilloscopeIntf {
public:
    Oscilloscope(float* trigger_src, float trigger_threshold, float** data_src, float** data_src2)
        : trigger_src_(trigger_src), trigger_threshold_(trigger_threshold), data_src_(data_src) , data_src2_(data_src2){}

    float get_val(uint32_t index, uint32_t column) override {
        return index < OSCILLOSCOPE_SIZE ? data_[index][column] : NAN;
    }

    void update();

    const uint32_t size_ = OSCILLOSCOPE_SIZE;
    const float* trigger_src_;
    const float trigger_threshold_;
    float* const * data_src_;
    float* const * data_src2_;

    float data_[OSCILLOSCOPE_SIZE][2] = {0};
    size_t pos_ = 0;
    bool ready_ = false;
    bool capturing_ = false;

    #if 0
    // this is from: https://stackoverflow.com/questions/1659440/32-bit-to-16-bit-floating-point-conversion
    typedef unsigned short ushort;
    typedef unsigned int uint;

    inline uint as_uint(const float x) {
        union { float f; uint i; } val;
        val.f = x;
        return val.i;
    }
    inline float as_float(const uint x) {
        union { float f; uint i; } val;
        val.i = x;
        return val.f;
    }

    inline float half_to_float(ushort x) { // IEEE-754 16-bit floating-point format (without infinity): 1-5-10, exp-15, +-131008.0, +-6.1035156E-5, +-5.9604645E-8, 3.311 digits
        const uint e = (x&0x7C00)>>10; // exponent
            const uint m = (x&0x03FF)<<13; // mantissa
        const uint v = as_uint((float)m)>>23; // evil log2 bit hack to count leading zeros in denormalized format
        return as_float((x&0x8000)<<16 | (e!=0)*((e+112)<<23|m) | ((e==0)&(m!=0))*((v-37)<<23|((m<<(150-v))&0x007FE000))); // sign : normalized : denormalized
    }
    inline ushort float_to_half(float x) { // IEEE-754 16-bit floating-point format (without infinity): 1-5-10, exp-15, +-131008.0, +-6.1035156E-5, +-5.9604645E-8, 3.311 digits
        const uint b = as_uint(x)+0x00001000; // round-to-nearest-even: add last bit after truncated mantissa
        const uint e = (b&0x7F800000)>>23; // exponent
        const uint m = b&0x007FFFFF; // mantissa; in line below: 0x007FF000 = 0x00800000-0x00001000 = decimal indicator flag - initial rounding
        return (b&0x80000000)>>16 | (e>112)*((((e-112)<<10)&0x7C00)|m>>13) | ((e<113)&(e>101))*((((0x007FF000+m)>>(125-e))+1)>>1) | (e>143)*0x7FFF; // sign : normalized : denormalized : saturate
    }

    using oscilloscope_type = ushort;
    #else
    using oscilloscope_type = float;
    inline float half_to_float(float x) {
        return x;
    }
    inline float float_to_half(float x) {
        return x;
    }
    #endif
};

#endif // __OSCILLOSCOPE_HPP