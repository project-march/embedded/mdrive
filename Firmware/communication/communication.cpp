
/* Includes ------------------------------------------------------------------*/

#include <fibre/async_stream.hpp>
#include "communication.h"

#include "interface_usb.h"
#include "interface_can.hpp"
#include "interface_i2c.h"
#include "interface_OrbisUART.h"
#include "interface_AksimBISS.h"
#include "interface_MantracourtUART.h"
#include "interface_ethercat.h"

#include "odrive_main.h"
#include "freertos_vars.h"
#include "utils.hpp"

#include <cmsis_os.h>
#include <memory>
#include <usbd_cdc_if.h>
#include <usb_device.h>
#include <usart.h>
#include <gpio.h>

#include <type_traits>

/* Private defines -----------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Global constant data ------------------------------------------------------*/
/* Global variables ----------------------------------------------------------*/

uint64_t serial_number;
char serial_number_str[13]; // 12 digits + null termination

/* Private constant data -----------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Function implementations --------------------------------------------------*/

/* SOES configuration */
static esc_cfg_t ecat_config = { 
    NULL,                   // user_arg
    0,                 // use_interrupt
    500,                // watchdog_cnt
    NULL,          // set_defaults_hook
    NULL,      // pre_state_change_hook
    NULL,     // post_state_change_hook
    NULL,           // application_hook
    NULL,        // safeoutput_override
    NULL,   // pre_object_download_hook
    NULL,  // post_object_download_hook
    NULL,             // rxpdo_override
    NULL,             // txpdo_override
    NULL,    // esc_hw_interrupt_enable
    NULL,   // esc_hw_interrupt_disable
    NULL,         // esc_hw_eep_handler
    NULL        // esc_check_dc_handler
};


void init_communication(void) {

    // Initialize EtherCAT slave
    ecat_slv_init(&ecat_config);

    start_mantracourt_server();
    start_mantracourt_thread();
    start_orbis_server();
    start_aksim_thread();
    start_usb_server();
    
    if (odrv.config_.enable_i2c_a) {
        start_i2c_server();
    }

    if (odrv.config_.enable_can_a) {
        odrv.can_.start_server(&hcan1);
    }
    
    start_ethercat_thread();
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef* huart) {
    if(huart == &huart1 | huart == &huart3) {
        Orbis_RxCpltCallback(huart);
    }
    else if(huart == &huart4) {
        mantracourt_RxCpltCallback(huart);
    }
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef* huart) {
    if(huart == &huart4) {
        mantracourt_TxCpltCallback(huart);
    }
}


extern "C" {
int _write(int file, const char* data, int len) __attribute__((used));
}

// @brief This is what printf calls internally
int _write(int file, const char* data, int len) {
    fibre::cbufptr_t buf{(const uint8_t*)data, (const uint8_t*)data + len};

    if (odrv.config_.usb_cdc_protocol == ODrive::STREAM_PROTOCOL_TYPE_STDOUT ||
        odrv.config_.usb_cdc_protocol == ODrive::STREAM_PROTOCOL_TYPE_ASCII_AND_STDOUT) {
        usb_cdc_stdout_sink.write(buf);
        if (!usb_cdc_stdout_pending) {
            usb_cdc_stdout_pending = true;
            osMessagePut(usb_event_queue, 7, 0);
        }
    }

    return len; // Always pretend that we processed everything
}


#include "../autogen/function_stubs.hpp"

ODrive& ep_root = odrv;
#include "../autogen/endpoints.hpp"
