#ifndef COMMUNICATION_H
#define COMMUNICATION_H

// TODO: resolve assert
#define assert(expr)

#ifdef __cplusplus

#include <functional>
#include <limits>

extern "C" {
#endif

#include <cmsis_os.h>
#include "soes/esc.h"
#include "soes/ecat_slv.h"
#include "soes/esi/utypes.h"

void init_communication(void);

#ifdef __cplusplus
}
#endif

#endif /* COMMANDS_H */
