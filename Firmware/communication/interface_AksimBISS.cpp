#include "interface_AksimBISS.h"

#include <MotorControl/utils.hpp>

#include <fibre/async_stream.hpp>
#include <fibre/../../legacy_protocol.hpp>
#include <spi.h>
#include <cmsis_os.h>
#include <freertos_vars.h>
#include <odrive_main.h>
#include "crc.hpp"
#include <iostream>
#include <mutex>


uint8_t read_buffer_aksim[12];
const uint32_t stack_size_aksim_thread = 512;
osThreadId aksim_thread;
uint32_t pos_abs_received;
uint8_t crc;
int pos_counter = 0;

static void aksim_polling_thread(void *)
{ 
    while(1) {
        TaskTimer::enabled = odrv.enable_task_timers_;

        MEASURE_TIME(axes[0].task_times_.aksim_poll) {
            HAL_SPI_Receive_IT(&hspi2, read_buffer_aksim, sizeof(read_buffer_aksim));

            pos_abs_received = ((read_buffer_aksim[3] & 0x7F) << 13) | (read_buffer_aksim[4] << 5) | ((read_buffer_aksim[5] & 0xF8) >> 5);
            crc = ((read_buffer_aksim[5] & 0x1) << 5) | (read_buffer_aksim[6] & 0xF8);

            if(crc != 0) {
                axes[0].encoder_.pos_abs_ethercat_ = pos_abs_received;

            
                if(!axes[0].encoder_.pos_abs_updated_) {

                    axes[0].encoder_.pos_abs_ = ((read_buffer_aksim[3] & 0x7F) << 13) | (read_buffer_aksim[4] << 5) | ((read_buffer_aksim[5] & 0xF8) >> 5);

                    if(pos_counter > 10) {
                        axes[0].encoder_.pos_abs_updated_ = true;
                    }

                    pos_counter++;
                }
            }
        }

        osDelay(5);
    }
}

void start_aksim_thread() {
    osThreadDef(aksim_thread_def, aksim_polling_thread, osPriorityAboveNormal, 0, stack_size_aksim_thread / sizeof(StackType_t));
    aksim_thread = osThreadCreate(osThread(aksim_thread_def), NULL);
}
