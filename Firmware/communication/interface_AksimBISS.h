#ifndef INC_AKSIMBISS_H_
#define INC_AKSIMBISS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <cmsis_os.h>
#include "usart.h"

extern osThreadId aksim_thread;
extern const uint32_t stack_size_aksim_thread;

void start_aksim_thread(void);
static void aksim_polling_thread(void *);

#ifdef __cplusplus
}
#endif

#endif // INC_AKSIMBISS_H_
