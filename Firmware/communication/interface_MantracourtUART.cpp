
#include "interface_MantracourtUART.h"

#include <MotorControl/utils.hpp>

#include <fibre/async_stream.hpp>
#include <fibre/../../legacy_protocol.hpp>
#include <usart.h>
#include <cmsis_os.h>
#include <freertos_vars.h>
#include <odrive_main.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


const uint32_t stack_size_mantracourt_thread = 512;
osThreadId mantracourt_thread;
uint8_t read_buffer_torque[15] = {0};
uint32_t torque_loop = 0;

void start_mantracourt_server() {
    HAL_UART_Receive_DMA(&huart4, read_buffer_torque, 15);
}

void mantracourt_RxCpltCallback(UART_HandleTypeDef* huart) {
    HAL_UART_Receive_DMA(&huart4, read_buffer_torque, 15);

    set_torque(read_buffer_torque);
}

void mantracourt_TxCpltCallback(UART_HandleTypeDef* huart) {
    HAL_GPIO_WritePin(TORQUE_DE_GPIO_Port, TORQUE_DE_Pin, GPIO_PIN_RESET);
}

void set_torque(uint8_t* read_buffer) {
    char* read_buffer_string;
    float torque;

    read_buffer_string = reinterpret_cast<char*>(read_buffer);
    sscanf(read_buffer_string, "%f", &torque);

    if((read_buffer_string[7] == '.')) {
        axes[1].torque_sensor_.raw_moment = torque;
    }
    else if((read_buffer_string[6] == '.')) {
        axes[0].torque_sensor_.raw_moment = torque;
    }
}

static void mantracourt_polling_thread(void *)
{ 
    while(1) {
        TaskTimer::enabled = odrv.enable_task_timers_;

        if(torque_loop % 4 == 0){
            MEASURE_TIME(axes[0].task_times_.torque_poll) {
                HAL_GPIO_WritePin(TORQUE_DE_GPIO_Port, TORQUE_DE_Pin, GPIO_PIN_SET);
                HAL_UART_Transmit_DMA(&huart4, write_buffer_rotational, sizeof(write_buffer_rotational));
            }
        }
        else if(torque_loop % 4 == 2){
            MEASURE_TIME(axes[1].task_times_.torque_poll) {
                HAL_GPIO_WritePin(TORQUE_DE_GPIO_Port, TORQUE_DE_Pin, GPIO_PIN_SET);
                HAL_UART_Transmit_DMA(&huart4, write_buffer_linear, sizeof(write_buffer_linear));
            }
        }

        torque_loop++;

        osDelay(5);
    }
}

void start_mantracourt_thread() {
    osThreadDef(mantracourt_thread_def, mantracourt_polling_thread, osPriorityNormal, 0, stack_size_mantracourt_thread / sizeof(StackType_t));
    mantracourt_thread = osThreadCreate(osThread(mantracourt_thread_def), NULL);
}