#ifndef INC_MANTRACOURTUART_H_
#define INC_MANTRACOURTUART_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <cmsis_os.h>
#include "usart.h"

#define read_buffer_torque_size 15
#define write_buffer_torque_size 11

extern osThreadId mantracourt_thread;
extern const uint32_t stack_size_mantracourt_thread;

void start_aksim_thread(void);
static void aksim_polling_thread(void *);

const uint8_t write_buffer_linear[write_buffer_torque_size] = {'!','0','0','1',':','S','O','U','T','?','\r'};
const uint8_t write_buffer_rotational[write_buffer_torque_size] = {'!','0','0','2',':','S','O','U','T','?','\r'};

void start_mantracourt_server();
void mantracourt_RxCpltCallback(UART_HandleTypeDef* huart);
void mantracourt_TxCpltCallback(UART_HandleTypeDef* huart);
void set_torque(uint8_t* read_buffer);
static void mantracourt_polling_thread(void *);
void start_mantracourt_thread(void);

#ifdef __cplusplus
}
#endif

#endif // INC_MANTRACOURTUART_H_
