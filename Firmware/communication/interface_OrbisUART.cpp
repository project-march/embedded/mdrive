
#include "interface_OrbisUART.h"

#include <MotorControl/utils.hpp>

#include <fibre/async_stream.hpp>
#include <fibre/../../legacy_protocol.hpp>
#include <usart.h>
#include <cmsis_os.h>
#include <freertos_vars.h>
#include <odrive_main.h>

uint8_t read_buffer_linear[3];
uint8_t read_buffer_AIE[3];

void start_orbis_server() {
    HAL_UART_Receive_DMA(&huart3, read_buffer_linear, 3);
    HAL_UART_Receive_DMA(&huart1, read_buffer_AIE, 3);

    axes[1].encoder_.pos_abs_ = (read_buffer_linear[1] << 6) | (read_buffer_linear[2] >> 2);
    axes[1].encoder_.orbis_status_ = read_buffer_linear[1] & 0x03;
    axes[1].encoder_.pos_abs_ethercat_ =  axes[1].encoder_.pos_abs_;

    axes[1].aie_encoder_.pos_abs_ = (read_buffer_AIE[1] << 6) | (read_buffer_AIE[2] >> 2);
    axes[1].aie_encoder_.status_ = read_buffer_AIE[1] & 0x03;
}

void orbis_poll() {
    uint8_t write_buffer = 0x31;

    HAL_UART_Transmit_DMA(&huart3, &write_buffer, 1);
    HAL_UART_Transmit_DMA(&huart1, &write_buffer, 1);
}

void Orbis_RxCpltCallback(UART_HandleTypeDef* huart) {
    if(huart == &huart3) {
        HAL_UART_Receive_DMA(&huart3, read_buffer_linear, 3);
        
        axes[1].encoder_.pos_abs_ethercat_ = (read_buffer_linear[1] << 6) | (read_buffer_linear[2] >> 2);

        if(!axes[1].encoder_.pos_abs_updated_ && (axes[1].current_state_ == ODriveIntf::AxisIntf::AxisState::AXIS_STATE_IDLE)) {
            setAbsPosOrbis(read_buffer_linear);

            axes[1].encoder_.pos_abs_updated_ = true;
        }
    }
    else if(huart == &huart1) {
        HAL_UART_Receive_DMA(&huart1, read_buffer_linear, 3);
        
        axes[1].aie_encoder_.pos_abs_ = (read_buffer_AIE[1] << 6) | (read_buffer_AIE[2] >> 2);
    }
}

void setAbsPosOrbis(uint8_t* read_buffer) {
    axes[1].encoder_.pos_abs_ = (read_buffer[1] << 6) | (read_buffer[2] >> 2);
    axes[1].encoder_.orbis_status_ = read_buffer[1] & 0x03;
}
