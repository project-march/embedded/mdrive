#ifndef INC_ORBISUART_H_
#define INC_ORBISUART_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <cmsis_os.h>
#include "usart.h"

void start_orbis_server();
void orbis_poll(void);
void Orbis_RxCpltCallback(UART_HandleTypeDef* huart);
void setAbsPosOrbis(uint8_t* read_buffer);
// uint8_t uart_poll(bool ready);
// void requestPosition(void);

#ifdef __cplusplus
}
#endif

#endif // __INTERFACE_UART_HPP
