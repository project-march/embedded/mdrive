#include "interface_ethercat.h"
#include <MotorControl/utils.hpp>

#include <cmsis_os.h>
#include <freertos_vars.h>
#include <odrive_main.h>
#include "autogen/interfaces.hpp"

const uint32_t stack_size_ethercat_poll_thread = 1024;
const uint32_t stack_size_ethercat_update_thread = 512;
osThreadId ethercat_poll_thread;
osThreadId ethercat_update_thread;
bool ecat_network_interrupt = false;

//EtherCAT defines

/* CANopen Object Dictionary */
_Objects    Obj;

// Dummy functions, otherwise ecat_slv.c will not compile
void cb_get_inputs(){};
void cb_set_outputs(){};

// Outputs 
void cb_get_miso()
{
    Obj.TxPDO.Axis0AbsolutePosition = axes[0].encoder_.pos_abs_ethercat_;
    Obj.TxPDO.Axis0Current = axes[0].motor_.current_control_.Iq_measured_;
    Obj.TxPDO.Axis0MotorVelocity = axes[0].encoder_.vel_estimate_.any().value_or(0.0f);
    Obj.TxPDO.Axis0ODriveError = odrv.error_;
    Obj.TxPDO.Axis0AxisError = axes[0].error_;
    Obj.TxPDO.Axis0MotorError = axes[0].motor_.error_;
    Obj.TxPDO.Axis0EncoderError = axes[0].encoder_.error_;
    Obj.TxPDO.Axis0TorqueSensorError = axes[0].torque_sensor_.error_;
    Obj.TxPDO.Axis0ControllerError = axes[0].controller_.error_;
    Obj.TxPDO.Axis0State = axes[0].current_state_;
    Obj.TxPDO.Axis0ODriveTemperature = 0;
    Obj.TxPDO.Axis0MotorTemperature = axes[0].motor_.fet_thermistor_.temperature_;
    Obj.TxPDO.Axis0ShadowCount = axes[0].encoder_.shadow_count_;
    Obj.TxPDO.Axis0Torque = axes[0].torque_sensor_.moment_estimate_.any().value_or(0.0f);
    Obj.TxPDO.Axis1AbsolutePosition = axes[1].encoder_.pos_abs_ethercat_;
    Obj.TxPDO.Axis1Current = axes[1].motor_.current_control_.Iq_measured_;
    Obj.TxPDO.Axis1MotorVelocity = axes[1].encoder_.vel_estimate_.any().value_or(0.0f);
    Obj.TxPDO.Axis1ODriveError = odrv.error_;
    Obj.TxPDO.Axis1AxisError = axes[1].error_;
    Obj.TxPDO.Axis1MotorError = axes[1].motor_.error_;
    Obj.TxPDO.Axis1EncoderError = axes[1].encoder_.error_;
    Obj.TxPDO.Axis1TorqueSensorError = axes[1].torque_sensor_.error_;
    Obj.TxPDO.Axis1ControllerError = axes[1].controller_.error_;
    Obj.TxPDO.Axis1State = axes[1].current_state_;
    Obj.TxPDO.Axis1ODriveTemperature = 0;
    Obj.TxPDO.Axis1MotorTemperature = axes[1].motor_.fet_thermistor_.temperature_;
    Obj.TxPDO.Axis1ShadowCount = axes[1].encoder_.shadow_count_;
    Obj.TxPDO.Axis1Torque = axes[1].torque_sensor_.moment_estimate_.any().value_or(0.0f);
    Obj.TxPDO.AIEAbsolutePosition = axes[1].aie_encoder_.pos_abs_rad_.any().value_or(0.0f);
}

// Inputs
void cb_set_mosi()
{
    axes[0].controller_.input_moment_ = Obj.RxPDO.Axis0TargetTorque;
    axes[0].controller_.input_pos_ = Obj.RxPDO.Axis0TargetPosition;
    axes[0].controller_.weight_position_ = Obj.RxPDO.Axis0FuzzyPosition;
    axes[0].controller_.weight_moment_ = Obj.RxPDO.Axis0FuzzyTorque;
    
    axes[0].controller_.config_.pos_gain = Obj.RxPDO.Axis0PositionP;
    axes[0].controller_.config_.vel_integrator_gain = Obj.RxPDO.Axis0PositionI;
    axes[0].controller_.config_.vel_gain = Obj.RxPDO.Axis0PostionD;
    axes[0].controller_.config_.moment_gain = Obj.RxPDO.Axis0TorqueP;
    axes[0].controller_.config_.moment_deriv_gain = Obj.RxPDO.Axis0TorqueD;

    axes[1].controller_.input_moment_ = Obj.RxPDO.Axis1TargetTorque;
    axes[1].controller_.input_pos_ = Obj.RxPDO.Axis1TargetPosition;
    axes[1].controller_.weight_position_ = Obj.RxPDO.Axis1FuzzyPosition;
    axes[1].controller_.weight_moment_ = Obj.RxPDO.Axis1FuzzyTorque;

    axes[1].controller_.config_.pos_gain = Obj.RxPDO.Axis1PositionP;
    axes[1].controller_.config_.vel_integrator_gain = Obj.RxPDO.Axis1PositionI;
    axes[1].controller_.config_.vel_gain = Obj.RxPDO.Axis1PositionD;
    axes[1].controller_.config_.moment_gain = Obj.RxPDO.Axis1TorqueP;
    axes[1].controller_.config_.moment_deriv_gain = Obj.RxPDO.Axis1TorqueD;
}

void ecat_interrupt(){
  ecat_network_interrupt = true;
}

static void ethercat_polling_thread(void *)
{ 
    while(1) {
        TaskTimer::enabled = odrv.enable_task_timers_;

        MEASURE_TIME(odrv.task_times_.ethercat_poll) {
            ecat_slv();
        }
        osDelay(5);
    }
}

static void ethercat_object_update_thread(void *)
{ 
    while(1) {
        TaskTimer::enabled = odrv.enable_task_timers_;

        MEASURE_TIME(odrv.task_times_.ethercat_update_loop) {
            cb_get_miso();  

            if(odrv.config_.enable_ethercat) {
                cb_set_mosi();   
            }

            if (!((ODriveIntf::AxisIntf::AxisState) (Obj.RxPDO.Axis0RequestedState) == axes[0].last_requested_state_ ) && Obj.RxPDO.Axis0RequestedState != 0) 
            {
                axes[0].requested_state_ = (ODriveIntf::AxisIntf::AxisState) (Obj.RxPDO.Axis0RequestedState);
            }

            if (!((ODriveIntf::AxisIntf::AxisState) (Obj.RxPDO.Axis1RequestedState) == axes[1].last_requested_state_ ) && Obj.RxPDO.Axis1RequestedState != 0) 
            {
                axes[1].requested_state_ = (ODriveIntf::AxisIntf::AxisState) (Obj.RxPDO.Axis1RequestedState);
            }
                
            axes[0].last_requested_state_ = (ODriveIntf::AxisIntf::AxisState) (Obj.RxPDO.Axis0RequestedState);
            axes[1].last_requested_state_ = (ODriveIntf::AxisIntf::AxisState) (Obj.RxPDO.Axis1RequestedState);
        }

        osDelay(5);
    }
}

void start_ethercat_thread() {
    osThreadDef(ethercat_thread_def, ethercat_polling_thread, osPriorityNormal, 0, stack_size_ethercat_poll_thread / sizeof(StackType_t));
    ethercat_poll_thread = osThreadCreate(osThread(ethercat_thread_def), NULL);

    osThreadDef(ethercat_update_thread_def, ethercat_object_update_thread, osPriorityNormal, 0, stack_size_ethercat_update_thread / sizeof(StackType_t));
    ethercat_update_thread = osThreadCreate(osThread(ethercat_update_thread_def), NULL);
}