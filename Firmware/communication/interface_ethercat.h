#ifndef __INTERFACE_ETHERCAT_H
#define __INTERFACE_ETHERCAT_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <cmsis_os.h>
#include "soes/esc.h"
#include "soes/ecat_slv.h"
#include "soes/esi/utypes.h"

extern osThreadId ethercat_poll_thread;
extern osThreadId ethercat_update_thread;
extern const uint32_t stack_size_ethercat_poll_thread;
extern const uint32_t stack_size_ethercat_update_thread;

#ifdef __cplusplus
}
#endif

void cb_get_inputs();
void cb_set_outputs();
void start_ethercat_thread(void);
static void ethercat_polling_thread(void *);
static void ethercat_object_update_thread(void *);

// Dummy functions, otherwise ecat_slv.c will not compile
void cb_get_miso();
void cb_set_mosi();


#endif //__INTERFACE_ETHERCAT_H
