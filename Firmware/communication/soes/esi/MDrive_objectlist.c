#include "../esc_coe.h"
#include "utypes.h"
#include <stddef.h>

#ifndef HW_REV
#define HW_REV "1.0"
#endif

#ifndef SW_REV
#define SW_REV "1.0"
#endif

static const char acName1000[] = "Device Type";
static const char acName1008[] = "Manufacturer Device Name";
static const char acName1009[] = "Manufacturer Hardware Version";
static const char acName100A[] = "Manufacturer Software Version";
static const char acName1018[] = "Identity Object";
static const char acName1018_00[] = "Max SubIndex";
static const char acName1018_01[] = "Vendor ID";
static const char acName1018_02[] = "Product Code";
static const char acName1018_03[] = "Revision Number";
static const char acName1018_04[] = "Serial Number";
static const char acName1600[] = "RxPDO";
static const char acName1600_00[] = "Max SubIndex";
static const char acName1600_01[] = "Axis0TargetTorque";
static const char acName1600_02[] = "Axis0TargetPosition";
static const char acName1600_03[] = "Axis0FuzzyTorque";
static const char acName1600_04[] = "Axis0FuzzyPosition";
static const char acName1600_05[] = "Axis0PositionP";
static const char acName1600_06[] = "Axis0PositionI";
static const char acName1600_07[] = "Axis0PostionD";
static const char acName1600_08[] = "Axis0TorqueP";
static const char acName1600_09[] = "Axis0TorqueD";
static const char acName1600_0A[] = "Axis0RequestedState";
static const char acName1600_0B[] = "Axis1TargetTorque";
static const char acName1600_0C[] = "Axis1TargetPosition";
static const char acName1600_0D[] = "Axis1FuzzyTorque";
static const char acName1600_0E[] = "Axis1FuzzyPosition";
static const char acName1600_0F[] = "Axis1PositionP";
static const char acName1600_10[] = "Axis1PositionI";
static const char acName1600_11[] = "Axis1PositionD";
static const char acName1600_12[] = "Axis1TorqueP";
static const char acName1600_13[] = "Axis1TorqueD";
static const char acName1600_14[] = "Axis1RequestedState";
static const char acName1600_15[] = "ChecksumMOSI";
static const char acName1A00[] = "TxPDO";
static const char acName1A00_00[] = "Max SubIndex";
static const char acName1A00_01[] = "Axis0AbsolutePosition";
static const char acName1A00_02[] = "Axis0Current";
static const char acName1A00_03[] = "Axis0MotorVelocity";
static const char acName1A00_04[] = "Axis0ODriveError";
static const char acName1A00_05[] = "Axis0AxisError";
static const char acName1A00_06[] = "Axis0MotorError";
static const char acName1A00_07[] = "Axis0EncoderError";
static const char acName1A00_08[] = "Axis0TorqueSensorError";
static const char acName1A00_09[] = "Axis0ControllerError";
static const char acName1A00_0A[] = "Axis0State";
static const char acName1A00_0B[] = "Axis0ODriveTemperature";
static const char acName1A00_0C[] = "Axis0MotorTemperature";
static const char acName1A00_0D[] = "Axis0ShadowCount";
static const char acName1A00_0E[] = "Axis0Torque";
static const char acName1A00_0F[] = "Axis1AbsolutePosition";
static const char acName1A00_10[] = "Axis1Current";
static const char acName1A00_11[] = "Axis1MotorVelocity";
static const char acName1A00_12[] = "Axis1ODriveError";
static const char acName1A00_13[] = "Axis1AxisError";
static const char acName1A00_14[] = "Axis1MotorError";
static const char acName1A00_15[] = "Axis1EncoderError";
static const char acName1A00_16[] = "Axis1TorqueSensorError";
static const char acName1A00_17[] = "Axis1ControllerError";
static const char acName1A00_18[] = "Axis1State";
static const char acName1A00_19[] = "Axis1ODriveTemperature";
static const char acName1A00_1A[] = "Axis1MotorTemperature";
static const char acName1A00_1B[] = "Axis1ShadowCount";
static const char acName1A00_1C[] = "Axis1Torque";
static const char acName1A00_1D[] = "AIEAbsolutePosition";
static const char acName1A00_1E[] = "ChecksumMISO";
static const char acName1A00_1F[] = "CheckChecksumMOSI";
static const char acName1C00[] = "Sync Manager Communication Type";
static const char acName1C00_00[] = "Max SubIndex";
static const char acName1C00_01[] = "Communications Type SM0";
static const char acName1C00_02[] = "Communications Type SM1";
static const char acName1C00_03[] = "Communications Type SM2";
static const char acName1C00_04[] = "Communications Type SM3";
static const char acName1C12[] = "Sync Manager 2 PDO Assignment";
static const char acName1C12_00[] = "Max SubIndex";
static const char acName1C12_01[] = "PDO Mapping";
static const char acName1C13[] = "Sync Manager 3 PDO Assignment";
static const char acName1C13_00[] = "Max SubIndex";
static const char acName1C13_01[] = "PDO Mapping";
static const char acName6000[] = "TxPDO";
static const char acName6000_00[] = "Max SubIndex";
static const char acName6000_01[] = "Axis0AbsolutePosition";
static const char acName6000_02[] = "Axis0Current";
static const char acName6000_03[] = "Axis0MotorVelocity";
static const char acName6000_04[] = "Axis0ODriveError";
static const char acName6000_05[] = "Axis0AxisError";
static const char acName6000_06[] = "Axis0MotorError";
static const char acName6000_07[] = "Axis0EncoderError";
static const char acName6000_08[] = "Axis0TorqueSensorError";
static const char acName6000_09[] = "Axis0ControllerError";
static const char acName6000_0A[] = "Axis0State";
static const char acName6000_0B[] = "Axis0ODriveTemperature";
static const char acName6000_0C[] = "Axis0MotorTemperature";
static const char acName6000_0D[] = "Axis0ShadowCount";
static const char acName6000_0E[] = "Axis0Torque";
static const char acName6000_0F[] = "Axis1AbsolutePosition";
static const char acName6000_10[] = "Axis1Current";
static const char acName6000_11[] = "Axis1MotorVelocity";
static const char acName6000_12[] = "Axis1ODriveError";
static const char acName6000_13[] = "Axis1AxisError";
static const char acName6000_14[] = "Axis1MotorError";
static const char acName6000_15[] = "Axis1EncoderError";
static const char acName6000_16[] = "Axis1TorqueSensorError";
static const char acName6000_17[] = "Axis1ControllerError";
static const char acName6000_18[] = "Axis1State";
static const char acName6000_19[] = "Axis1ODriveTemperature";
static const char acName6000_1A[] = "Axis1MotorTemperature";
static const char acName6000_1B[] = "Axis1ShadowCount";
static const char acName6000_1C[] = "Axis1Torque";
static const char acName6000_1D[] = "AIEAbsolutePosition";
static const char acName6000_1E[] = "ChecksumMISO";
static const char acName6000_1F[] = "CheckChecksumMOSI";
static const char acName7000[] = "RxPDO";
static const char acName7000_00[] = "Max SubIndex";
static const char acName7000_01[] = "Axis0TargetTorque";
static const char acName7000_02[] = "Axis0TargetPosition";
static const char acName7000_03[] = "Axis0FuzzyTorque";
static const char acName7000_04[] = "Axis0FuzzyPosition";
static const char acName7000_05[] = "Axis0PositionP";
static const char acName7000_06[] = "Axis0PositionI";
static const char acName7000_07[] = "Axis0PostionD";
static const char acName7000_08[] = "Axis0TorqueP";
static const char acName7000_09[] = "Axis0TorqueD";
static const char acName7000_0A[] = "Axis0RequestedState";
static const char acName7000_0B[] = "Axis1TargetTorque";
static const char acName7000_0C[] = "Axis1TargetPosition";
static const char acName7000_0D[] = "Axis1FuzzyTorque";
static const char acName7000_0E[] = "Axis1FuzzyPosition";
static const char acName7000_0F[] = "Axis1PositionP";
static const char acName7000_10[] = "Axis1PositionI";
static const char acName7000_11[] = "Axis1PositionD";
static const char acName7000_12[] = "Axis1TorqueP";
static const char acName7000_13[] = "Axis1TorqueD";
static const char acName7000_14[] = "Axis1RequestedState";
static const char acName7000_15[] = "ChecksumMOSI";
static const char acName8000[] = "TxSDO";
static const char acName8000_00[] = "Max SubIndex";
static const char acName8000_01[] = "Axis0MinimumPosition";
static const char acName8000_02[] = "Axis0MaximumPosition";
static const char acName8000_03[] = "Axis0ZeroPosition";
static const char acName8000_04[] = "Axis1MinimumPosition";
static const char acName8000_05[] = "Axis1MaximumPosition";
static const char acName8000_06[] = "Axis1ZeroPosition";
static const char acName8000_07[] = "AIEMinimumPosition";
static const char acName8000_08[] = "AIEMaximumPosition";
static const char acName8000_09[] = "AIEZeroPosition";

const _objd SDO1000[] =
{
  {0x0, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1000, 0x00001389, NULL},
};
const _objd SDO1008[] =
{
  {0x0, DTYPE_VISIBLE_STRING, 48, ATYPE_RO, acName1008, 0, "MDrive"},
};
const _objd SDO1009[] =
{
  {0x0, DTYPE_VISIBLE_STRING, 24, ATYPE_RO, acName1009, 0, HW_REV},
};
const _objd SDO100A[] =
{
  {0x0, DTYPE_VISIBLE_STRING, 24, ATYPE_RO, acName100A, 0, SW_REV},
};
const _objd SDO1018[] =
{
  {0x00, DTYPE_UNSIGNED8, 8, ATYPE_RO, acName1018_00, 4, NULL},
  {0x01, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1018_01, 0x94C, NULL},
  {0x02, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1018_02, 0, NULL},
  {0x03, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1018_03, 0, NULL},
  {0x04, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1018_04, 0x00000000, &Obj.serial},
};
const _objd SDO1600[] =
{
  {0x00, DTYPE_UNSIGNED8, 8, ATYPE_RO, acName1600_00, 21, NULL},
  {0x01, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_01, 0x70000120, NULL},
  {0x02, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_02, 0x70000220, NULL},
  {0x03, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_03, 0x70000320, NULL},
  {0x04, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_04, 0x70000420, NULL},
  {0x05, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_05, 0x70000520, NULL},
  {0x06, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_06, 0x70000620, NULL},
  {0x07, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_07, 0x70000720, NULL},
  {0x08, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_08, 0x70000820, NULL},
  {0x09, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_09, 0x70000920, NULL},
  {0x0A, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_0A, 0x70000A20, NULL},
  {0x0B, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_0B, 0x70000B20, NULL},
  {0x0C, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_0C, 0x70000C20, NULL},
  {0x0D, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_0D, 0x70000D20, NULL},
  {0x0E, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_0E, 0x70000E20, NULL},
  {0x0F, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_0F, 0x70000F20, NULL},
  {0x10, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_10, 0x70001020, NULL},
  {0x11, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_11, 0x70001120, NULL},
  {0x12, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_12, 0x70001220, NULL},
  {0x13, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_13, 0x70001320, NULL},
  {0x14, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_14, 0x70001420, NULL},
  {0x15, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1600_15, 0x70001520, NULL},
};
const _objd SDO1A00[] =
{
  {0x00, DTYPE_UNSIGNED8, 8, ATYPE_RO, acName1A00_00, 31, NULL},
  {0x01, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_01, 0x60000120, NULL},
  {0x02, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_02, 0x60000220, NULL},
  {0x03, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_03, 0x60000320, NULL},
  {0x04, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_04, 0x60000420, NULL},
  {0x05, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_05, 0x60000520, NULL},
  {0x06, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_06, 0x60000640, NULL},
  {0x07, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_07, 0x60000720, NULL},
  {0x08, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_08, 0x60000820, NULL},
  {0x09, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_09, 0x60000920, NULL},
  {0x0A, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_0A, 0x60000A20, NULL},
  {0x0B, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_0B, 0x60000B20, NULL},
  {0x0C, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_0C, 0x60000C20, NULL},
  {0x0D, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_0D, 0x60000D20, NULL},
  {0x0E, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_0E, 0x60000E20, NULL},
  {0x0F, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_0F, 0x60000F20, NULL},
  {0x10, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_10, 0x60001020, NULL},
  {0x11, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_11, 0x60001120, NULL},
  {0x12, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_12, 0x60001220, NULL},
  {0x13, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_13, 0x60001320, NULL},
  {0x14, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_14, 0x60001440, NULL},
  {0x15, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_15, 0x60001520, NULL},
  {0x16, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_16, 0x60001620, NULL},
  {0x17, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_17, 0x60001720, NULL},
  {0x18, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_18, 0x60001820, NULL},
  {0x19, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_19, 0x60001920, NULL},
  {0x1A, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_1A, 0x60001A20, NULL},
  {0x1B, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_1B, 0x60001B20, NULL},
  {0x1C, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_1C, 0x60001C20, NULL},
  {0x1D, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_1D, 0x60001D20, NULL},
  {0x1E, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_1E, 0x60001E20, NULL},
  {0x1F, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName1A00_1F, 0x60001F20, NULL},
};
const _objd SDO1C00[] =
{
  {0x00, DTYPE_UNSIGNED8, 8, ATYPE_RO, acName1C00_00, 4, NULL},
  {0x01, DTYPE_UNSIGNED8, 8, ATYPE_RO, acName1C00_01, 1, NULL},
  {0x02, DTYPE_UNSIGNED8, 8, ATYPE_RO, acName1C00_02, 2, NULL},
  {0x03, DTYPE_UNSIGNED8, 8, ATYPE_RO, acName1C00_03, 3, NULL},
  {0x04, DTYPE_UNSIGNED8, 8, ATYPE_RO, acName1C00_04, 4, NULL},
};
const _objd SDO1C12[] =
{
  {0x00, DTYPE_UNSIGNED8, 8, ATYPE_RO, acName1C12_00, 1, NULL},
  {0x01, DTYPE_UNSIGNED16, 16, ATYPE_RO, acName1C12_01, 0x1600, NULL},
};
const _objd SDO1C13[] =
{
  {0x00, DTYPE_UNSIGNED8, 8, ATYPE_RO, acName1C13_00, 1, NULL},
  {0x01, DTYPE_UNSIGNED16, 16, ATYPE_RO, acName1C13_01, 0x1A00, NULL},
};
const _objd SDO6000[] =
{
  {0x00, DTYPE_UNSIGNED8, 8, ATYPE_RO, acName6000_00, 31, NULL},
  {0x01, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_01, 0, &Obj.TxPDO.Axis0AbsolutePosition},
  {0x02, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_02, 0x00000000, &Obj.TxPDO.Axis0Current},
  {0x03, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_03, 0x00000000, &Obj.TxPDO.Axis0MotorVelocity},
  {0x04, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_04, 0, &Obj.TxPDO.Axis0ODriveError},
  {0x05, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_05, 0, &Obj.TxPDO.Axis0AxisError},
  {0x06, DTYPE_UNSIGNED64, 64, ATYPE_RO | ATYPE_TXPDO, acName6000_06, 0, &Obj.TxPDO.Axis0MotorError},
  {0x07, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_07, 0, &Obj.TxPDO.Axis0EncoderError},
  {0x08, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_08, 0, &Obj.TxPDO.Axis0TorqueSensorError},
  {0x09, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_09, 0, &Obj.TxPDO.Axis0ControllerError},
  {0x0A, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_0A, 0, &Obj.TxPDO.Axis0State},
  {0x0B, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_0B, 0x00000000, &Obj.TxPDO.Axis0ODriveTemperature},
  {0x0C, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_0C, 0x00000000, &Obj.TxPDO.Axis0MotorTemperature},
  {0x0D, DTYPE_INTEGER32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_0D, 0, &Obj.TxPDO.Axis0ShadowCount},
  {0x0E, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_0E, 0x00000000, &Obj.TxPDO.Axis0Torque},
  {0x0F, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_0F, 0, &Obj.TxPDO.Axis1AbsolutePosition},
  {0x10, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_10, 0x00000000, &Obj.TxPDO.Axis1Current},
  {0x11, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_11, 0x00000000, &Obj.TxPDO.Axis1MotorVelocity},
  {0x12, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_12, 0, &Obj.TxPDO.Axis1ODriveError},
  {0x13, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_13, 0, &Obj.TxPDO.Axis1AxisError},
  {0x14, DTYPE_UNSIGNED64, 64, ATYPE_RO | ATYPE_TXPDO, acName6000_14, 0, &Obj.TxPDO.Axis1MotorError},
  {0x15, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_15, 0, &Obj.TxPDO.Axis1EncoderError},
  {0x16, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_16, 0, &Obj.TxPDO.Axis1TorqueSensorError},
  {0x17, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_17, 0, &Obj.TxPDO.Axis1ControllerError},
  {0x18, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_18, 0, &Obj.TxPDO.Axis1State},
  {0x19, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_19, 0x00000000, &Obj.TxPDO.Axis1ODriveTemperature},
  {0x1A, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_1A, 0x00000000, &Obj.TxPDO.Axis1MotorTemperature},
  {0x1B, DTYPE_INTEGER32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_1B, 0, &Obj.TxPDO.Axis1ShadowCount},
  {0x1C, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_1C, 0x00000000, &Obj.TxPDO.Axis1Torque},
  {0x1D, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_1D, 0x00000000, &Obj.TxPDO.AIEAbsolutePosition},
  {0x1E, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_1E, 0, &Obj.TxPDO.ChecksumMISO},
  {0x1F, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_TXPDO, acName6000_1F, 0, &Obj.TxPDO.CheckChecksumMOSI},
};
const _objd SDO7000[] =
{
  {0x00, DTYPE_UNSIGNED8, 8, ATYPE_RO, acName7000_00, 21, NULL},
  {0x01, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_01, 0x00000000, &Obj.RxPDO.Axis0TargetTorque},
  {0x02, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_02, 0x00000000, &Obj.RxPDO.Axis0TargetPosition},
  {0x03, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_03, 0x00000000, &Obj.RxPDO.Axis0FuzzyTorque},
  {0x04, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_04, 0x00000000, &Obj.RxPDO.Axis0FuzzyPosition},
  {0x05, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_05, 0x00000000, &Obj.RxPDO.Axis0PositionP},
  {0x06, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_06, 0x00000000, &Obj.RxPDO.Axis0PositionI},
  {0x07, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_07, 0x00000000, &Obj.RxPDO.Axis0PostionD},
  {0x08, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_08, 0x00000000, &Obj.RxPDO.Axis0TorqueP},
  {0x09, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_09, 0x00000000, &Obj.RxPDO.Axis0TorqueD},
  {0x0A, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_0A, 0, &Obj.RxPDO.Axis0RequestedState},
  {0x0B, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_0B, 0x00000000, &Obj.RxPDO.Axis1TargetTorque},
  {0x0C, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_0C, 0x00000000, &Obj.RxPDO.Axis1TargetPosition},
  {0x0D, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_0D, 0x00000000, &Obj.RxPDO.Axis1FuzzyTorque},
  {0x0E, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_0E, 0x00000000, &Obj.RxPDO.Axis1FuzzyPosition},
  {0x0F, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_0F, 0x00000000, &Obj.RxPDO.Axis1PositionP},
  {0x10, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_10, 0x00000000, &Obj.RxPDO.Axis1PositionI},
  {0x11, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_11, 0x00000000, &Obj.RxPDO.Axis1PositionD},
  {0x12, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_12, 0x00000000, &Obj.RxPDO.Axis1TorqueP},
  {0x13, DTYPE_REAL32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_13, 0x00000000, &Obj.RxPDO.Axis1TorqueD},
  {0x14, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_14, 0, &Obj.RxPDO.Axis1RequestedState},
  {0x15, DTYPE_UNSIGNED32, 32, ATYPE_RO | ATYPE_RXPDO, acName7000_15, 0, &Obj.RxPDO.ChecksumMOSI},
};
const _objd SDO8000[] =
{
  {0x00, DTYPE_UNSIGNED8, 8, ATYPE_RO, acName8000_00, 9, NULL},
  {0x01, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName8000_01, 0, &Obj.TxSDO.Axis0MinimumPosition},
  {0x02, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName8000_02, 0, &Obj.TxSDO.Axis0MaximumPosition},
  {0x03, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName8000_03, 0, &Obj.TxSDO.Axis0ZeroPosition},
  {0x04, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName8000_04, 0, &Obj.TxSDO.Axis1MinimumPosition},
  {0x05, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName8000_05, 0, &Obj.TxSDO.Axis1MaximumPosition},
  {0x06, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName8000_06, 0, &Obj.TxSDO.Axis1ZeroPosition},
  {0x07, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName8000_07, 0, &Obj.TxSDO.AIEMinimumPosition},
  {0x08, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName8000_08, 0, &Obj.TxSDO.AIEMaximumPosition},
  {0x09, DTYPE_UNSIGNED32, 32, ATYPE_RO, acName8000_09, 0, &Obj.TxSDO.AIEZeroPosition},
};

const _objectlist SDOobjects[] =
{
  {0x1000, OTYPE_VAR, 0, 0, acName1000, SDO1000},
  {0x1008, OTYPE_VAR, 0, 0, acName1008, SDO1008},
  {0x1009, OTYPE_VAR, 0, 0, acName1009, SDO1009},
  {0x100A, OTYPE_VAR, 0, 0, acName100A, SDO100A},
  {0x1018, OTYPE_RECORD, 4, 0, acName1018, SDO1018},
  {0x1600, OTYPE_RECORD, 21, 0, acName1600, SDO1600},
  {0x1A00, OTYPE_RECORD, 31, 0, acName1A00, SDO1A00},
  {0x1C00, OTYPE_ARRAY, 4, 0, acName1C00, SDO1C00},
  {0x1C12, OTYPE_ARRAY, 1, 0, acName1C12, SDO1C12},
  {0x1C13, OTYPE_ARRAY, 1, 0, acName1C13, SDO1C13},
  {0x6000, OTYPE_RECORD, 31, 0, acName6000, SDO6000},
  {0x7000, OTYPE_RECORD, 21, 0, acName7000, SDO7000},
  {0x8000, OTYPE_RECORD, 9, 0, acName8000, SDO8000},
  {0xffff, 0xff, 0xff, 0xff, NULL, NULL}
};
