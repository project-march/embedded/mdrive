// #include "spi_lan.hpp"
// // #include "esc_hw.h"

// Stm32Gpio cs_lan = {ESC_GPIOX_CS, ESC_GPIO_Pin_CS};
// Stm32SpiArbiter* spi_lan_arbiter;
// Stm32SpiArbiter::SpiTask lan_spi_task_;
// uint8_t lan_spi_dma_tx_[2] = {0xFF, 0xFF};
// uint8_t lan_spi_dma_rx_[5];


// void spi_setup(void)
// {   
//     lan_spi_task_.config = {
//       .Mode = SPI_MODE_MASTER,
//       .Direction = SPI_DIRECTION_2LINES,
//       .DataSize = SPI_DATASIZE_8BIT,
//       .CLKPolarity = SPI_POLARITY_LOW,
//       .CLKPhase = SPI_PHASE_1EDGE,
//       .NSS = SPI_NSS_SOFT,
//       .BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4,
//       .FirstBit = SPI_FIRSTBIT_MSB,
//       .TIMode = SPI_TIMODE_DISABLE,
//       .CRCCalculation = SPI_CRCCALCULATION_DISABLE,
//       .CRCPolynomial = 7,
//     };
// }

// inline static uint8_t spi_transfer_byte(uint8_t byte)
// {
//     uint8_t rx_buff;

//     if(!((lan_spi_task_.config.Mode == hspi3.Init.Mode)
//       && (lan_spi_task_.config.Direction == hspi3.Init.Direction)
//       && (lan_spi_task_.config.DataSize == hspi3.Init.DataSize)
//       && (lan_spi_task_.config.CLKPolarity == hspi3.Init.CLKPolarity)
//       && (lan_spi_task_.config.CLKPhase == hspi3.Init.CLKPhase)
//       && (lan_spi_task_.config.NSS == hspi3.Init.NSS)
//       && (lan_spi_task_.config.BaudRatePrescaler == hspi3.Init.BaudRatePrescaler)
//       && (lan_spi_task_.config.FirstBit == hspi3.Init.FirstBit)
//       && (lan_spi_task_.config.TIMode == hspi3.Init.TIMode)
//       && (lan_spi_task_.config.CRCCalculation == hspi3.Init.CRCCalculation)
//       && (lan_spi_task_.config.CRCPolynomial == hspi3.Init.CRCPolynomial))) {
//         HAL_SPI_DeInit(&hspi3);
//         hspi3.Init = lan_spi_task_.config;
//         HAL_SPI_Init(&hspi3);
//         __HAL_SPI_ENABLE(&hspi3);
//     }
  
// 	HAL_SPI_TransmitReceive(&hspi3, &byte, &rx_buff, 1, 100);

//     return rx_buff;
// }

// void write_ (int8_t board, uint8_t *data, uint8_t size)
// {    
//     for (uint8_t i = 0; i < size; i++)
//     {
//         spi_transfer_byte(data[i]);
//     }
// }

// void read_ (int8_t board, uint8_t *result, uint8_t size)
// {
//     for (uint8_t i = 0; i < size; i++)
//     {
//         result[i] = spi_transfer_byte(DUMMY_BYTE);
//     }
// }
