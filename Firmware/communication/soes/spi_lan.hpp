// #ifndef SPI_LAN_HPP_
// #define SPI_LAN_HPP_

// #include <stdint.h>
// #include <Drivers/STM32/stm32_spi_arbiter.hpp>
// #include <Drivers/STM32/stm32_gpio.hpp>

// #define SCS_LOW                           0
// #define SCS_HIGH                          1
// #define SCS_ACTIVE_POLARITY               SCS_LOW

// #define SPIX_ESC                          SPI3
// #define SPIX_ESC_SPEED                    18000000

// /**SPI3 GPIO Configuration
// PA2      ------> LAN_CS
// PC10     ------> SPI2_SCK
// PC11     ------> SPI2_MISO
// PB12     ------> SPI2_MOSI
// */

// #define ESC_GPIOX_CS                    GPIOA
// #define ESC_GPIO_Pin_CS                 GPIO_PIN_2

// #define ESC_GPIOX_CTRL                  GPIOC
// #define ESC_GPIO_Pin_SCK                GPIO_PIN_10
// #define ESC_GPIO_Pin_MISO               GPIO_PIN_11
// #define ESC_GPIO_Pin_MOSI               GPIO_PIN_12

// #define SPIX_ESC_SCS                    SPI_NSS_Soft
// #define SPIX_ESC_CPOL                   SPI_CPOL_Low
// #define SPIX_ESC_CPHA                   SPI_CPHA_1Edge

// #define DUMMY_BYTE 0xFF
// #define SPI_MODE0 0

// extern Stm32Gpio cs_lan;
// extern Stm32SpiArbiter* spi_lan_arbiter;

// extern Stm32SpiArbiter::SpiTask lan_spi_task_;
// extern uint8_t lan_spi_dma_tx_[2];
// extern uint8_t lan__spi_dma_rx_[5];
// extern size_t lan_spi_dma_length_;

// extern "C" void spi_setup(void);
// extern "C" void spi_select ();
// extern "C" void spi_unselect ();
// extern "C" void write_ (int8_t board, uint8_t *data, uint8_t size);
// extern "C" void read_ (int8_t board, uint8_t *result, uint8_t size);
// extern "C" void spi_bidirectionally_transfer (int8_t board, uint8_t *result, uint8_t *data, uint8_t size);
// extern "C" void spi_release_task ();

// #endif /* SPI_LAN_HPP_ */
