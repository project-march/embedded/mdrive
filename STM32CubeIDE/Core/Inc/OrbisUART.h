/*
 * OrbisUART.h
 *
 *  Created on: Jan 9, 2024
 *      Author: Kim Hernandez Salvador (MIX)
 *
 */

#include <stdint.h>
#include <sys/types.h>
#include "usart.h"

#ifndef INC_ORBISUART_H_
#define INC_ORBISUART_H_

#define POS_READ_BUFFER 3
#define STATUS_READ_BUFFER 4
#define TEMP_READ_BUFFER 5
#define MAX_READ_BUFFER 5

#define POS_REQUEST 0x31
#define STATUS_REQUEST 0x64
#define TEMP_REQUEST 0x74


class OrbisUART {
    private:
		UART_HandleTypeDef* m_huart;
        uint16_t m_abs_position;
        uint8_t m_encoder_status;
        int16_t m_temperature;
        uint8_t m_write_buffer;
        uint8_t m_read_buffer[MAX_READ_BUFFER];

        /**
         * @brief Read entire buffer and store last received messages
         *
         * @return 0 if bytes have been read; 1 if nothing is read
         */
        const void receiveData();

    public:

        //constructor
        OrbisUART(UART_HandleTypeDef* huart);
        //destructor
        ~OrbisUART() = default;

		bool m_receive_success;

        /**
         * @brief Request the position from the Orbis
         *
         * @return HAL status
         */

        const void requestPosition();

        /**
         * @brief Request the status from the Orbis
         *
         * @return HAL status
         */

        const void requestStatus();

        /**
         * @brief Request the temperature from the Orbis
         *
         * @return HAL status
         */

        const void requestTemperature();

        /**
         * @brief Returns last received position
         *
         * @return 2 byte position value
         */
        uint16_t getPosition();

        /**
         * @brief Returns encoder status
         *
         * @return 1 bytes of information about the encoder status
         */
        uint8_t getEncoderStatus();

        /**
         * @brief Returns temperature of Orbis.
         *        Temperature is multiplied by 10 with a tolerance of +-5 degrees
         *
         * @return 2 bytes of temperature information
         */
        uint16_t getTemperature();

};

#endif /* INC_ORBISUART_H_ */
