/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define TIM_1_8_PERIOD_CLOCKS 3500
#define TIM_APB1_CLOCK_HZ 84000000
#define TIM_APB1_DEADTIME_CLOCKS 40
#define TIM_APB1_PERIOD_CLOCKS 4096
#define TIM_1_8_RCR 2
#define TIM_1_8_DEADTIME_CLOCKS 20
#define TIM_1_8_CLOCK_HZ 168000000
#define configAPPLICATION_ALLOCATED_HEAP 1
#define M0_nCS_Pin GPIO_PIN_13
#define M0_nCS_GPIO_Port GPIOC
#define M1_nCS_Pin GPIO_PIN_14
#define M1_nCS_GPIO_Port GPIOC
#define M1_ENC_Z_Pin GPIO_PIN_15
#define M1_ENC_Z_GPIO_Port GPIOC
#define M0_IB_Pin GPIO_PIN_0
#define M0_IB_GPIO_Port GPIOC
#define M0_IC_Pin GPIO_PIN_1
#define M0_IC_GPIO_Port GPIOC
#define M1_IC_Pin GPIO_PIN_2
#define M1_IC_GPIO_Port GPIOC
#define M1_IB_Pin GPIO_PIN_3
#define M1_IB_GPIO_Port GPIOC
#define TORQUE_TX_Pin GPIO_PIN_0
#define TORQUE_TX_GPIO_Port GPIOA
#define TORQUE_RX_Pin GPIO_PIN_1
#define TORQUE_RX_GPIO_Port GPIOA
#define LAN_CS_Pin GPIO_PIN_2
#define LAN_CS_GPIO_Port GPIOA
#define nFAULT_Pin GPIO_PIN_3
#define nFAULT_GPIO_Port GPIOA
#define M1_TEMP_Pin GPIO_PIN_4
#define M1_TEMP_GPIO_Port GPIOA
#define M1_AL_Pin GPIO_PIN_5
#define M1_AL_GPIO_Port GPIOA
#define VBUS_S_Pin GPIO_PIN_6
#define VBUS_S_GPIO_Port GPIOA
#define AUX_TEMP_Pin GPIO_PIN_4
#define AUX_TEMP_GPIO_Port GPIOC
#define M0_TEMP_Pin GPIO_PIN_5
#define M0_TEMP_GPIO_Port GPIOC
#define M1_BL_Pin GPIO_PIN_0
#define M1_BL_GPIO_Port GPIOB
#define M1_CL_Pin GPIO_PIN_1
#define M1_CL_GPIO_Port GPIOB
#define M0_AL_Pin GPIO_PIN_8
#define M0_AL_GPIO_Port GPIOE
#define M0_AH_Pin GPIO_PIN_9
#define M0_AH_GPIO_Port GPIOE
#define M0_BL_Pin GPIO_PIN_10
#define M0_BL_GPIO_Port GPIOE
#define M0_BH_Pin GPIO_PIN_11
#define M0_BH_GPIO_Port GPIOE
#define M0_CL_Pin GPIO_PIN_12
#define M0_CL_GPIO_Port GPIOE
#define M0_CH_Pin GPIO_PIN_13
#define M0_CH_GPIO_Port GPIOE
#define AUX_L_Pin GPIO_PIN_10
#define AUX_L_GPIO_Port GPIOB
#define AUX_H_Pin GPIO_PIN_11
#define AUX_H_GPIO_Port GPIOB
#define EN_GATE_Pin GPIO_PIN_12
#define EN_GATE_GPIO_Port GPIOB
#define M0_MA_Pin GPIO_PIN_13
#define M0_MA_GPIO_Port GPIOB
#define M0_SLO_Pin GPIO_PIN_14
#define M0_SLO_GPIO_Port GPIOB
#define M1_TX_Pin GPIO_PIN_8
#define M1_TX_GPIO_Port GPIOD
#define M1_RX_Pin GPIO_PIN_9
#define M1_RX_GPIO_Port GPIOD
#define M1_AH_Pin GPIO_PIN_6
#define M1_AH_GPIO_Port GPIOC
#define M1_BH_Pin GPIO_PIN_7
#define M1_BH_GPIO_Port GPIOC
#define M1_CH_Pin GPIO_PIN_8
#define M1_CH_GPIO_Port GPIOC
#define M0_ENC_Z_Pin GPIO_PIN_9
#define M0_ENC_Z_GPIO_Port GPIOC
#define AIE_TX_Pin GPIO_PIN_9
#define AIE_TX_GPIO_Port GPIOA
#define AIE_RX_Pin GPIO_PIN_10
#define AIE_RX_GPIO_Port GPIOA
#define TORQUE_DE_Pin GPIO_PIN_15
#define TORQUE_DE_GPIO_Port GPIOA
#define SPI_SCK_Pin GPIO_PIN_10
#define SPI_SCK_GPIO_Port GPIOC
#define SPI_MISO_Pin GPIO_PIN_11
#define SPI_MISO_GPIO_Port GPIOC
#define SPI_MOSI_Pin GPIO_PIN_12
#define SPI_MOSI_GPIO_Port GPIOC
#define LAN_SYNC0_Pin GPIO_PIN_2
#define LAN_SYNC0_GPIO_Port GPIOD
#define LAN_SYNC1_Pin GPIO_PIN_3
#define LAN_SYNC1_GPIO_Port GPIOD
#define LAN_IRQ_Pin GPIO_PIN_4
#define LAN_IRQ_GPIO_Port GPIOD
#define M0_TX_Pin GPIO_PIN_5
#define M0_TX_GPIO_Port GPIOD
#define M0_RX_Pin GPIO_PIN_6
#define M0_RX_GPIO_Port GPIOD
#define M0_ENC_A_Pin GPIO_PIN_4
#define M0_ENC_A_GPIO_Port GPIOB
#define M0_ENC_B_Pin GPIO_PIN_5
#define M0_ENC_B_GPIO_Port GPIOB
#define M1_ENC_A_Pin GPIO_PIN_6
#define M1_ENC_A_GPIO_Port GPIOB
#define M1_ENC_B_Pin GPIO_PIN_7
#define M1_ENC_B_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
