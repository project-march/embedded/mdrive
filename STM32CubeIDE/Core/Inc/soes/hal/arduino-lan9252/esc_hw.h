#ifndef ESC_HW_H_
#define ESC_HW_H_

#include <stdint.h>
#include "stm32f4xx_hal.h"


#define SCS_LOW                           0
#define SCS_HIGH                          1
#define SCS_ACTIVE_POLARITY               SCS_LOW
#define SPIX_ESC_SPEED                    18000000

/**SPI3 GPIO Configuration
PA2      ------> LAN_CS
PC10     ------> SPI2_SCK
PC11     ------> SPI2_MISO
PB12     ------> SPI2_MOSI
*/

#define SPIX_ESC                        SPI3

#define ESC_GPIOX_CS                    GPIOA
#define ESC_GPIO_Pin_CS                 GPIO_PIN_2

#define ESC_GPIOX_CTRL                  GPIOC
#define ESC_GPIO_Pin_SCK                GPIO_PIN_10
#define ESC_GPIO_Pin_MISO               GPIO_PIN_11
#define ESC_GPIO_Pin_MOSI               GPIO_PIN_12

#define SPIX_ESC_SCS                    SPI_NSS_Soft
#define SPIX_ESC_CPOL                   SPI_CPOL_Low
#define SPIX_ESC_CPHA                   SPI_CPHA_1Edge


#define DUMMY_BYTE 0xFF
#define tout 5000

// void spi_setup(void);
void write_ (int8_t board, uint8_t *data, uint8_t size);
void read_ (int8_t board, uint8_t *result, uint8_t size);
uint8_t spi_transfer_byte(uint8_t byte);


#endif /* SRC_APP_SPI_H_ */
