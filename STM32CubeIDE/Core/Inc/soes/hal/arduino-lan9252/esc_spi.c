///*
// * esc_spi.c
// *
// *  Created on: Jan 17, 2024
// *      Author: kimhernandezsalvador
// */
//
//#include "esc_spi.h"
//#include "esc_hw.h"
//
//
//void spi_select (int8_t board)
//{
//    #if SCS_ACTIVE_POLARITY == SCS_LOW
//	  HAL_GPIO_WritePin(SPI2_CS_GPIO_Port, SPI2_CS_Pin, GPIO_PIN_RESET);
//    #endif
//}
//
//// Drive CS pin high to stop communication
//void spi_unselect (int8_t board)
//{
//    #if SCS_ACTIVE_POLARITY == SCS_LOW
//	  HAL_GPIO_WritePin(SPI2_CS_GPIO_Port, SPI2_CS_Pin, GPIO_PIN_SET);
//    #endif
//}
//
//// Transmit and receive data on spi line
//inline static uint8_t spi_transfer(uint8_t byte)
//{
//	uint8_t rx_buff;
//	HAL_SPI_TransmitReceive(&hspi2, &byte, &rx_buff, 1, 100);
////	HAL_SPI_Transmit(&hspi2, &byte, 1, 100);
//	return rx_buff;
//}
//
//void write_ (int8_t board, uint8_t *data, uint8_t size)
//{
//    for(int i = 0; i < size; ++i)
//    {
//        spi_transfer(data[i]);
//    }
//}
//
//void read_ (int8_t board, uint8_t *result, uint8_t size)
//{
//	for(int i = 0; i < size; ++i)
//    {
//        result[i] = spi_transfer(DUMMY_BYTE);
//    }
//}
