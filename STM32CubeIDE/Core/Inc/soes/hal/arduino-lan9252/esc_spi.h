///*
// * esc_spi.h
// *
// *  Created on: Jan 17, 2024
// *      Author: kimhernandezsalvador
// */
//
//#include "spi.h"
//
//
//#ifndef INC_SOES_HAL_ARDUINO_LAN9252_ESC_SPI_H_
//#define INC_SOES_HAL_ARDUINO_LAN9252_ESC_SPI_H_
//
//void spi_select (int8_t board);
//void spi_unselect (int8_t board);
//inline static uint8_t spi_transfer(uint8_t byte);
//void write_ (int8_t board, uint8_t *data, uint8_t size);
//void read_ (int8_t board, uint8_t *result, uint8_t size);
//
//#endif /* INC_SOES_HAL_ARDUINO_LAN9252_ESC_SPI_H_ */
