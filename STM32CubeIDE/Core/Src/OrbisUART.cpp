/*
 * OrbisUART.cpp
 *
 *  Created on: Jan 9, 2024
 *      Author: Kim Hernandez Salvador (MIX)
 *
 */

#include "OrbisUART.h"
#include "main.h"
#include <string.h>

OrbisUART::OrbisUART(UART_HandleTypeDef* huart) {
	m_huart = huart;
}

const void OrbisUART::receiveData() {
	m_receive_success = true;

	switch (m_write_buffer) {

		case POS_REQUEST:
			HAL_UARTEx_ReceiveToIdle_DMA(m_huart, m_read_buffer, POS_READ_BUFFER);

			if(m_read_buffer[0] != POS_REQUEST) {
				m_receive_success = false;
				break;
			}

			m_abs_position = ((m_read_buffer[2] & 0xFC) >> 2) | (m_read_buffer[1] << 6);
			m_encoder_status = m_read_buffer[1] & 0x03;
			break;

		case STATUS_REQUEST:
			HAL_UARTEx_ReceiveToIdle_DMA(m_huart, m_read_buffer, STATUS_READ_BUFFER);

			if(m_read_buffer[0] != STATUS_REQUEST) {
				m_receive_success = false;
				break;
			}

			m_abs_position = ((m_read_buffer[2] & 0xFC) >> 2) | (m_read_buffer[1] << 6);
			m_encoder_status = (m_read_buffer[1] & 0x03) | (m_read_buffer[3] & 0xFC);
			break;

		case TEMP_REQUEST:
			HAL_UARTEx_ReceiveToIdle_DMA(m_huart, m_read_buffer, TEMP_READ_BUFFER);

			if(m_read_buffer[0] != TEMP_REQUEST) {
				m_receive_success = false;
				break;
			}

			m_abs_position = ((m_read_buffer[2] & 0xFC) >> 2) | (m_read_buffer[1] << 6);
			m_encoder_status = m_read_buffer[1] & 0x03;
			m_temperature = (m_read_buffer[3] << 8) | (m_read_buffer[4]);
			break;

		default:
			m_receive_success = false;
			HAL_UART_AbortReceive(m_huart);
			break;
	}
}

const void OrbisUART::requestPosition() {
	m_write_buffer = POS_REQUEST;

	HAL_UART_Transmit_DMA(m_huart, &m_write_buffer, 1);
}

const void OrbisUART::requestStatus() {
	m_write_buffer = STATUS_REQUEST;

	HAL_UART_Transmit_DMA(m_huart, &m_write_buffer, 1);
}

const void OrbisUART::requestTemperature() {
	m_write_buffer = TEMP_REQUEST;

	HAL_UART_Transmit_DMA(m_huart, &m_write_buffer, 1);
}

uint16_t OrbisUART::getPosition() {
	receiveData();

	return m_abs_position;
}

uint8_t OrbisUART::getEncoderStatus() {
	receiveData();

	return m_encoder_status;
}

uint16_t OrbisUART::getTemperature() {
	receiveData();

	return m_temperature;
}
