/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "adc.h"
#include "can.h"
#include "dma.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "usb_otg.h"
#include "gpio.h"
#include "hal/arduino-lan9252/esc_hw.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#include "esc.h"
#include "ecat_slv.h"
#include "utypes.h"

#ifdef __cplusplus
}
#endif // __cplusplus

_Objects    Obj;

void cb_get_inputs()
{

}

void cb_set_outputs()
{

}

/* SOES configuration */
static esc_cfg_t ecat_config = {
    .user_arg = NULL,
    .use_interrupt = 0,
    .watchdog_cnt = 500,
    .set_defaults_hook = NULL,
    .pre_state_change_hook = NULL,
    .post_state_change_hook = NULL,
    .application_hook = NULL,
    .safeoutput_override = NULL,
    .pre_object_download_hook = NULL,
    .post_object_download_hook = NULL,
    .rxpdo_override = NULL,
    .txpdo_override = NULL,
    .esc_hw_interrupt_enable = NULL,
    .esc_hw_interrupt_disable = NULL,
    .esc_hw_eep_handler = NULL,
    .esc_check_dc_handler = NULL,
};

bool ecat_network_interrupt = false;

void ecat_interrupt(){
  ecat_network_interrupt = true;
}

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint8_t write_buffer[11] = {'!','0','0','2',':','S','O','U','T','?','\r'};
//uint8_t write_buffer[11] = {'!','0','0','1',':','S','O','U','T','?','\r'};
uint8_t write_buffer_dpb[11] = {'!','0','0','1',':','D','P','B','=','5','\r'};
uint8_t write_buffer_dp[10] = {'!','0','0','1',':','D','P','=','7','\r'};
uint8_t write_buffer_stn[11] = {'!','0','0','1',':','S','T','N','=','2','\r'};
uint8_t write_buffer_rst[9] = {'!','0','0','1',':','R','S','T','\r'};
//uint8_t write_buffer = 0x31;
uint8_t receive_buffer[15];
uint8_t received_data;
uint8_t receive_rotational[12];
uint64_t rawDataPacket;
uint8_t inverted_crc;
uint8_t calculated_crc;
uint8_t status;
uint8_t test;
uint32_t abs_pos;
uint32_t abs_pos_real;
uint32_t reg1;
uint32_t reg2;
uint32_t reg3;
float torque;

uint8_t ab_CRC6_LUT[64] = {
		0x00, 0x03, 0x06, 0x05, 0x0C, 0x0F, 0x0A, 0x09,
		 0x18, 0x1B, 0x1E, 0x1D, 0x14, 0x17, 0x12, 0x11,
		 0x30, 0x33, 0x36, 0x35, 0x3C, 0x3F, 0x3A, 0x39,
		 0x28, 0x2B, 0x2E, 0x2D, 0x24, 0x27, 0x22, 0x21,
		 0x23, 0x20, 0x25, 0x26, 0x2F, 0x2C, 0x29, 0x2A,
		 0x3B, 0x38, 0x3D, 0x3E, 0x37, 0x34, 0x31, 0x32,
		 0x13, 0x10, 0x15, 0x16, 0x1F, 0x1C, 0x19, 0x1A,
		 0x0B, 0x08, 0x0D, 0x0E, 0x07, 0x04, 0x01, 0x02};


/*32-bit input data, right alignment, Calculation over 24 bits (mult. of 6) */
uint8_t CRC_BiSS_43_42bit(uint64_t dw_InputData)
{
 uint8_t b_Index = 0;
 uint8_t b_CRC = 0;
 b_Index = (uint8_t)((dw_InputData >> 36u) & (uint64_t)0x00000003Fu);
 b_CRC = (uint8_t)((dw_InputData >> 30u) & (uint64_t)0x0000003Fu);
 b_Index = b_CRC ^ ab_CRC6_LUT[b_Index];

 b_CRC = (uint8_t)((dw_InputData >> 24u) & (uint64_t)0x0000003Fu);
 b_Index = b_CRC ^ ab_CRC6_LUT[b_Index];

 b_CRC = (uint8_t)((dw_InputData >> 18u) & (uint64_t)0x0000003Fu);
 b_Index = b_CRC ^ ab_CRC6_LUT[b_Index];

 b_CRC = (uint8_t)((dw_InputData >> 12u) & (uint64_t)0x0000003Fu);
 b_Index = b_CRC ^ ab_CRC6_LUT[b_Index];

 b_CRC = (uint8_t)((dw_InputData >> 6u) & (uint64_t)0x0000003Fu);
 b_Index = b_CRC ^ ab_CRC6_LUT[b_Index];

 b_CRC = (uint8_t)(dw_InputData & (uint64_t)0x0000003Fu);
 b_Index = b_CRC ^ ab_CRC6_LUT[b_Index];

 b_CRC = ab_CRC6_LUT[b_Index];

 return b_CRC;
}

//uint8_t crcBiSS(uint32_t data)
// {
//	uint8_t crc;
//	uint32_t tmp;
// tmp = (data >> 30) & 0x00000003;
// crc = ((data >> 24) & 0x0000003F);
// tmp = crc ^ ab_CRC6_LUT[tmp];
// crc = ((data >> 18) & 0x0000003F);
// tmp = crc ^ ab_CRC6_LUT[tmp];
// crc = ((data >> 12) & 0x0000003F);
// tmp = crc ^ ab_CRC6_LUT[tmp];
// crc = ((data >> 6) & 0x0000003F);
// tmp = crc ^ ab_CRC6_LUT[tmp];
// crc = (data & 0x0000003F);
// tmp = crc ^ ab_CRC6_LUT[tmp];
// crc = ab_CRC6_LUT[tmp];
// return crc;
// }

void set_torque(uint8_t* read_buffer) {
    char read_buffer_string[15];
    memcpy(read_buffer_string, read_buffer, 15);

    torque = atof(read_buffer_string);
}


void HAL_UART_RxCpltCallback(UART_HandleTypeDef* huart_) {
	if(huart_ == &huart1) {
		HAL_UART_Receive_DMA(&huart1, receive_buffer, 3);
	}
//	else if(huart_ == &huart4) {
//		HAL_UART_Receive_DMA(&huart4, receive_buffer, sizeof(receive_buffer));
//		set_torque(receive_buffer);
//	}
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef* huart_) {
	if(huart_ == &huart4) {
		HAL_GPIO_WritePin(TORQUE_DE_GPIO_Port, TORQUE_DE_Pin, GPIO_PIN_RESET);

		HAL_UART_Receive_DMA(&huart4, receive_buffer, sizeof(receive_buffer));
		set_torque(receive_buffer);
	}
}


/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */

	HAL_Init();


  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_ADC2_Init();
  MX_CAN1_Init();
  MX_TIM1_Init();
  MX_TIM8_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_SPI3_Init();
  MX_ADC3_Init();
  MX_TIM2_Init();
  MX_TIM13_Init();
  MX_UART4_Init();
  MX_SPI2_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  MX_USB_OTG_FS_PCD_Init();

  ecat_slv_init(&ecat_config);

  HAL_Delay(3000);

//  HAL_UART_Receive_DMA(&huart1, receive_buffer, 3);
//    HAL_UART_Receive_DMA(&huart4, receive_buffer, sizeof(receive_buffer));

//  HAL_GPIO_WritePin(TORQUE_DE_GPIO_Port, TORQUE_DE_Pin, GPIO_PIN_SET);
//  HAL_UART_Transmit(&huart4, write_buffer_dpb, sizeof(write_buffer_dpb), 100);
//  HAL_UART_Transmit(&huart4, write_buffer_dp, sizeof(write_buffer_dp), 100);
//  HAL_UART_Transmit(&huart4, write_buffer_stn, sizeof(write_buffer_stn), 100);
//  HAL_GPIO_WritePin(TORQUE_DE_GPIO_Port, TORQUE_DE_Pin, GPIO_PIN_RESET);




  while (1)
  {
	  ecat_slv();
	  HAL_Delay(10);

//	  HAL_SPI_Receive(&hspi2, receive_rotational, sizeof(receive_rotational), 10);

//	  HAL_Delay(100);

//	  HAL_UART_Transmit(&huart1, &write_buffer, sizeof(write_buffer), 100);
//	  HAL_UART_Transmit_DMA(&huart1, &write_buffer, sizeof(write_buffer));
//	  HAL_UART_Receive(&huart1, receive_buffer, 1, 100);
//	  HAL_UART_Transmit(&huart3, &write_buffer, 1, 100);

//	  HAL_GPIO_WritePin(TORQUE_DE_GPIO_Port, TORQUE_DE_Pin, GPIO_PIN_SET);
//	  HAL_UART_Transmit(&huart4, write_buffer, 11, 100);
//	  HAL_UART_Transmit_DMA(&huart4, write_buffer, 11);

//	  HAL_Delay(100);
//	  HAL_UART_Receive(&huart4, receive_buffer, 5, 100);
  }
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage
    */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks
    */
  // RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  // RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
	  Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV4;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
	  Error_Handler();
  }

  /**Configure the Systick interrupt time
  */
//  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  /**Configure the Systick
  */
//  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  // /* SysTick_IRQn interrupt configuration */
  // HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM14 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM14) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
