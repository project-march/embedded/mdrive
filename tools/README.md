# ODrive python tool

Command line tool to automate some things for setting up an [ODrive](https://odriverobotics.com/).

This tool offers multiple options that can be run independently:
- Erasing the configuration
- Setting configuration parameters
- Preparing the ODrive for actuation
- Performing a simple actuation test

## Usage

Find usage with:

`python3 main.py -h`