import argparse

import odrive
import sys

from src.mock_odrive import MockODrive
from src.odrive_class import ODrive
from src.odrive_controller import ODriveController
from src.util import make_config, make_joints

CONFIG_FOLDER = "config"

def parse_args():
    parser = argparse.ArgumentParser(
        description="CLI for configuring and actuating an ODrive."
    )
    parser.add_argument(
        "joints",
        nargs="+",
        help="Which joints to use. Can be full joint names or abbreviations.",
    )
    parser.add_argument(
        "-e",
        "--erase",
        action="store_true",
        help="Erase the configuration of the ODrive.",
    )
    parser.add_argument(
        "-i",
        "--set-idle",
        action="store_true",
        help="Set the ODrive in Idle mode.",
    )
    parser.add_argument(
        "-c",
        "--configure",
        action="store_true",
        help="Configure the ODrive, uses the general.yaml for general configuration and {joint_type}.yaml for each joint.",
    )
    parser.add_argument(
        "-t",
        "--set-pre-calibrated",
        action="store_true",
        help="Set pre_calibrated to True on the ODrive, after calibrating.",
    )
    parser.add_argument(
        "-p",
        "--prepare",
        action="store_true",
        help="Prepare the ODrive for actuation, perform calibration and set to closed loop control.",
    )
    parser.add_argument(
        "-s",
        "--skip-calibration",
        action="store_true",
        help="Skip calibration, only used if preparing for actuation.",
    )
    parser.add_argument(
        "-a",
        "--actuate",
        action="store_true",
        help="Actuate the ODrive, uses the actuation.yaml file for configuration.",
    )

    parser.add_argument(
        "-test",
        "--perform-autotuning",
        action="store_true",
        help="Perform autotuning on the ODrive.",
    )

    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    if not (
        args.erase
        or args.set_idle
        or args.configure
        or args.prepare
        or args.actuate
        or args.set_pre_calibrated
        or args.perform_autotuning
    ):
        print("Please select at least one option.")
        return
    config = make_config(CONFIG_FOLDER, args)
    joints = make_joints(args.joints, config["joints"])

    if len(joints) == 1 and joints[0].name == "mock_joint":
        odrive_entity = MockODrive()
    else:
        odrive_entity = odrive.find_any(timeout=ODrive.FIND_TIMEOUT)

    if odrive_entity is None:
        print(f"No ODrive found within {ODrive.FIND_TIMEOUT}s")
        return
    else:
        print(f"ODrive with serial number {odrive_entity.serial_number} found.")

    controller = ODriveController(odrive_entity, joints, config, args)
    controller.start()

if __name__ == "__main__":
    main()
