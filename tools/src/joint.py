from attr import dataclass

JOINT_ABBREVATIONS = {
    "la": "left_ankle",
    "laa": "left_hip_aa",
    "lfe": "left_hip_fe",
    "lk": "left_knee",
    "ra": "right_ankle",
    "raa": "right_hip_aa",
    "rfe": "right_hip_fe",
    "rk": "right_knee",
    "mock": "mock_joint",
    "tjrp": "test_joint_rotational_primary",
    "tjlp": "test_joint_linear_primary",
    "tjrs": "test_joint_rotational_secondary",
    "tjls": "test_joint_linear_secondary",
}


@dataclass
class Joint:
    name: str
    config: dict

    @staticmethod
    def make(name: str, all_joints_config: dict):
        if name in all_joints_config:
            return Joint(name, all_joints_config[name])

        for abbreviation, full_name in JOINT_ABBREVATIONS.items():
            if abbreviation == name:
                return Joint(full_name, all_joints_config[full_name])

        raise Exception("Unknown joint.")
