from attr import dataclass
from odrive.enums import AXIS_STATE_IDLE, AXIS_STATE_FULL_CALIBRATION_SEQUENCE


@dataclass
class MockEncoderConfig:
    # abs_spi_cs_gpio_pin: int = 1
    use_index: bool = False
    mode: int = 1
    cpr: int = 8192
    pre_calibrated: bool = False
    min_pos_abs: int = 0
    max_pos_abs: int = 8192


@dataclass
class MockEncoder:
    config: MockEncoderConfig = MockEncoderConfig()
    index_found: bool = False
    is_ready: bool = False
    error: int = 0
    shadow_count: int = 0
    pos_abs: int = 0

    def run_index_search(self):
        self.index_found = True

    def run_offset_calibration(self):
        self.is_ready = True


@dataclass
class MockMotorConfig:
    current_lim: float = 10.0
    calibration_current: float = 10.0
    resistance_calib_max_voltage: float = 2.0
    requested_current_range: float = 60.0
    torque_constant: float = 0.04
    pole_pairs: int = 7
    pre_calibrated: bool = False


@dataclass
class MockMotor:
    config: MockMotorConfig = MockMotorConfig()
    is_calibrated_: bool = False
    error: int = 0

    @property
    def is_calibrated(self):
        return self.is_calibrated_

    @is_calibrated.setter
    def is_calibrated(self, value):
        pass

    def run_calibration(self):
        self.is_calibrated_ = True


@dataclass
class MockControllerConfig:
    vel_limit: float = 2
    pos_gain: float = 20.0
    vel_gain: float = 1.0 / 6.0
    enable_current_mode_vel_limit: bool = True
    pre_calibrated: bool = False


@dataclass
class MockController:
    config: MockControllerConfig = MockControllerConfig()
    input_pos: float = 0.0
    input_vel: float = 0.0
    input_torque: float = 0.0
    error: int = 0


@dataclass
class MockAxisConfig:
    startup_closed_loop_control: bool = False


@dataclass
class MockAxis:
    config: MockAxisConfig = MockAxisConfig()
    encoder: MockEncoder = MockEncoder()
    controller: MockController = MockController()
    motor: MockMotor = MockMotor()
    current_state: int = AXIS_STATE_IDLE
    requested_state_: int = AXIS_STATE_IDLE
    error: int = 0

    @property
    def requested_state(self):
        return self.requested_state_

    @requested_state.setter
    def requested_state(self, state):
        self.requested_state_ = state
        if self.requested_state_ == AXIS_STATE_FULL_CALIBRATION_SEQUENCE:
            self.motor.run_calibration()
            if self.encoder.config.use_index:
                self.encoder.run_index_search()
            self.encoder.run_offset_calibration()
            self.current_state = AXIS_STATE_IDLE
        else:
            self.current_state = state


@dataclass
class MockGeneralConfig:
    enable_brake_resistor: bool = False
    brake_resistance: float = 2.0
    uart0_protocol: int = 3
    uart1_protocol: int = 3
    uart2_protocol: int = 3
    enable_uart_a: bool = True
    enable_uart_b: bool = False
    enable_uart_c: bool = False
    enable_can_a: bool = True
    enable_i2c_a: bool = False
    uart_a_baudrate: int = 115200


@dataclass
class MockODrive:
    config: MockGeneralConfig = MockGeneralConfig()
    serial_number: int = 0
    axis0: MockAxis = MockAxis()
    axis1: MockAxis = MockAxis()
    error: int = 0

    def clear_errors(self):
        pass

    def erase_configuration(self):
        pass

    def save_configuration(self):
        pass
