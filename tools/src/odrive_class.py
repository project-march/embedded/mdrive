import sys
import time
from typing import Optional, Any, List, Callable

from odrive.enums import (
    AXIS_STATE_CLOSED_LOOP_CONTROL,
    AXIS_STATE_FULL_CALIBRATION_SEQUENCE,
    CONTROL_MODE_TORQUE_CONTROL,
    CONTROL_MODE_VELOCITY_CONTROL,
    CONTROL_MODE_FUZZY_CONTROL,
    CONTROL_MODE_MOMENT_CONTROL,
    CONTROL_MODE_POSITION_CONTROL,
    AXIS_STATE_IDLE,
)
from odrive.utils import dump_errors
from odrive.utils import check_encoder
from odrive.utils import check_controller
from odrive.utils import IU_to_rads
from odrive.utils import move_through_range
from odrive.utils import calculate_zero_from_angle

from src.joint import Joint
from src.mock_odrive import MockODrive
from src.odrive_error import ODriveError


class ODrive:
    FIND_TIMEOUT = 5
    CALIBRATION_SLEEP_TIME = 5
    DEFAULT_COMM_WAIT = 1

    def __init__(self, odrive_entity, joints: List[Joint], config: dict, args):
        """Initialize the ODrive class."""
        self.odrive_entity = odrive_entity
        self.config = config
        self.args = args

        self.name = odrive_entity.serial_number
        self.control_mode = config["general"]["control_mode"]
        self.joints = joints
        self.axes = [joint.config["axis"] for joint in joints]

    def call_rebooting_function(self, function: Callable):
        """Call a function that reboots."""
        self.set_idle()
        print("Rebooting ODrive...")

        try:
            function()
        except Exception:
            print("Successfully rebooted ODrive.")
            sys.exit(0)

    def erase_configuration(self):
        self.call_rebooting_function(self.odrive_entity.erase_configuration)

    def save_configuration(self):
        self.call_rebooting_function(self.odrive_entity.save_configuration)

    def set_idle(self):
        for axis in self.axes:
            self.set_state(axis, AXIS_STATE_IDLE)

            if self.get_state(axis) == AXIS_STATE_IDLE:
                print(f"Axis {axis} successfully set to idle.")
            else:
                print(f"Axis {axis} failed to set to idle.")

    def apply_axis_config(self, joint, config_yaml):
        axis = self.get_axis(joint.config["axis"])
        for category in config_yaml:
            if category == "config":
                for key, value in config_yaml["config"].items():
                    setattr(axis.config, key, value)
            else:
                for key, value in config_yaml[category]["config"].items():
                    setattr(getattr(axis, category).config, key, value)
        axis.controller.config.control_mode = self.control_mode

    def set_window_filter(self, joint, absolute_encoder_cpr):
        axis = self.get_axis(joint.config["axis"])
        if joint.config["is_flipped"]:
            axis.encoder.config.min_pos_abs = (
                absolute_encoder_cpr - joint.config["max_pos_abs"]
            )
            axis.encoder.config.max_pos_abs = (
                absolute_encoder_cpr - joint.config["min_pos_abs"]
            )
        else:
            axis.encoder.config.min_pos_abs = joint.config["min_pos_abs"]
            axis.encoder.config.max_pos_abs = joint.config["max_pos_abs"]

    def set_encoder_index_search_direction(self, joint):
        axis = self.get_axis(joint.config["axis"])
        search_direction = joint.config["encoder_index_search_direction"]

        if search_direction == 1:
            if axis.config.calibration_lockin.ramp_distance < 0:
                axis.config.calibration_lockin.ramp_distance *= -1
            if axis.config.calibration_lockin.accel < 0:
                axis.config.calibration_lockin.accel *= -1
        elif search_direction == -1:
            if axis.config.calibration_lockin.ramp_distance > 0:
                axis.config.calibration_lockin.ramp_distance *= -1
            if axis.config.calibration_lockin.accel > 0:
                axis.config.calibration_lockin.accel *= -1

    def apply_config(self):
        if "general" in self.config and "config" in self.config["general"]:
            for key, value in self.config["general"]["config"].items():
                setattr(self.odrive_entity.config, key, value)
            print("Applied general config")
        for joint in self.joints:
            joint_type = joint.config["type"]
            self.apply_axis_config(joint, self.config[joint_type])
            print(
                f"Applied {joint.config['type']} config for {joint.name} on axis {joint.config['axis']}"
            )
            if "config" in joint.config:
                self.apply_axis_config(joint, joint.config["config"])
                print(
                    f"Applied custom config for {joint.name} on axis {joint.config['axis']}"
                )
            if "joint_specific_values" in joint.config:
                axis = self.get_axis(joint.config["axis"])
                encoder_config = joint.config["joint_specific_values"]["encoder"]["config"]
                axis.aie_encoder.config.enable_aie_encoder = joint.config["enable_aie_encoder"]
                if joint.config["enable_aie_encoder"]: 
                    aie_encoder_config = joint.config["joint_specific_values"]["aie_encoder"]["config"]
                self.apply_axis_config(joint, joint.config["joint_specific_values"])

                if "test" in joint.name:
                    axis.encoder.config.zero_position = (joint.config["max_pos_abs"] + joint.config["min_pos_abs"]) / 2

                elif "zero_position_angle" in encoder_config:
                    zero_iu_offset = round(encoder_config["zero_position_angle"]*encoder_config["cpr_absolute"]/(360))
                    axis.encoder.config.zero_position = round(joint.config["min_pos_abs"] + zero_iu_offset)

            if joint.config["enable_aie_encoder"]:
                print ("\nSetting AIE encoder limits")
                axis.aie_encoder.config.zero_position_aie = (aie_encoder_config["max_pos_abs_aie"] + aie_encoder_config["min_pos_abs_aie"]) / 2
                print(f"AIE zero position set to {axis.aie_encoder.config.zero_position_aie} ")
                print(f"AIE min pos abs set to {aie_encoder_config['min_pos_abs_aie']} ")
                print(f"AIE max pos abs set to {aie_encoder_config['max_pos_abs_aie']} ")
            
            print("\nSetting encoder limits")
            print(f"Zero position set to {axis.encoder.config.zero_position} ")
            print(f"Min pos abs set to {joint.config['min_pos_abs']} ")
            print(f"Max pos abs set to {joint.config['max_pos_abs']} ")
        
            
            self.set_window_filter(
                joint, self.config["general"]["absolute_encoder_cpr"][joint_type]
            )

            self.set_encoder_index_search_direction(joint)
        self.save_configuration()

    def set_pre_calibrated(self):
        for axis_num, axis in self.enumerate_axes():
            axis.encoder.config.use_index = True
            self.calibrate(axis_num)
            axis.motor.config.pre_calibrated = True
            axis.encoder.config.pre_calibrated = True
            
        for axis_num, axis in self.enumerate_axes():
            if not (
                axis.motor.is_calibrated
                and axis.motor.config.pre_calibrated
                and axis.encoder.is_ready
                and axis.encoder.config.pre_calibrated
            ):
                raise ODriveError(
                    self.odrive_entity,
                    f"Could not set axis {axis_num} to pre_calibrated",
                )
        print(f"Successfully set axes {self.axes} to pre_calibrated")
        self.save_configuration()

    def prepare(self):
        """Prepare the ODrive for actuation."""
        for axis in self.axes:
            if self.args.skip_calibration is None or not self.args.skip_calibration:
                self.calibrate(axis)
            self.set_state(axis, AXIS_STATE_CLOSED_LOOP_CONTROL)
            if not isinstance(self.odrive_entity, MockODrive):
                dump_errors(self.odrive_entity)
            if self.get_state(axis) == AXIS_STATE_CLOSED_LOOP_CONTROL:
                print(f"Prepared axis {axis} for actuation")
            else:
                print(f"Could not prepare axis {axis} for actuation")
        self.check_active()

    def calibrate(
        self, axis_num: int, sleep_time: Optional[int] = CALIBRATION_SLEEP_TIME
    ):
        """Set the axis state to AXIS_STATE_FULL_CALIBRATION_SEQUENCE and wait
        until calibration is done.

        :param axis_num axis to calibrate
        :param sleep_time Optional time to specify how long the thread
            should sleep before checking again
        """
        print(f"Calibrating axis{axis_num}")
        self.set_state(axis_num, AXIS_STATE_FULL_CALIBRATION_SEQUENCE)

        while not self.get_state(axis_num) == AXIS_STATE_IDLE:
            print("Waiting for calibration to finish...")
            self.wait(sleep_time)  

        if (self.get_axis(axis_num).error != 0):
            print("Calibration unsuccesfull")
            raise ODriveError(self.odrive_entity)

    def check_active(self):
        for axis in self.axes:
            if (
                self.get_axis(axis).error != 0
                or self.get_state(axis) != AXIS_STATE_CLOSED_LOOP_CONTROL
            ):
                raise ODriveError(self.odrive_entity)

    def wait(self, sleep_time: Optional[float] = None):
        time.sleep(sleep_time if sleep_time is not None else ODrive.DEFAULT_COMM_WAIT)

    def get_attribute(self, attribute: str, axis_num: Optional[int] = None) -> Any:
        """Get an attribute of the ODrive recursively.

        :param attribute Attribute to get
        :param axis_num Optional axis to get attribute of

        :return Returns the value of the attribute
        """
        attribute_tree = attribute.split(".")

        if axis_num is None:
            return_attribute = self.odrive_entity
        else:
            return_attribute = self.get_axis(axis_num)

        for attribute_part in attribute_tree:
            return_attribute = getattr(return_attribute, attribute_part)

        return return_attribute

    def set_attributes(self, attribute: str, value, axis_num: Optional[int] = None):
        """Set an attribute of the ODrive recursively.

        :param attribute Attribute to get
        :param axis_num Optional axis to get attribute of

        :return Returns the value of the attribute
        """
        attribute_tree = attribute.split(".")

        if axis_num is None:
            set_attribute = self.odrive_entity
        else:
            set_attribute = self.get_axis(axis_num)

        for attribute_part in attribute_tree[:-1]:
            set_attribute = getattr(set_attribute, attribute_part)

        to_type = type(getattr(set_attribute, attribute_tree[-1]))
        setattr(set_attribute, attribute_tree[-1], to_type(value))

    def get_axis(self, axis_num: int):
        """Get an axis of the ODrive with getattr().

        :param axis_num Axis to get.

        :return Returns the axis
        """
        return getattr(self.odrive_entity, f"axis{axis_num}")

    def set_velocity_iu(self, axis_num: int, velocity_iu: float):
        """Set the velocity internal units.

        :param axis_num Axis to set the torque of
        :param velocity_iu Torque to set
        """
        self.get_axis(axis_num).controller.input_vel = velocity_iu

    def set_actuation_target(self, axis_num: int, target: float):
        if self.control_mode == CONTROL_MODE_TORQUE_CONTROL:
            self.get_axis(axis_num).controller.input_torque = target
        elif self.control_mode == CONTROL_MODE_VELOCITY_CONTROL:
            self.get_axis(axis_num).controller.input_vel = target
        elif self.control_mode == CONTROL_MODE_FUZZY_CONTROL:
            self.get_axis(axis_num).controller.input_pos = target
        elif self.control_mode == CONTROL_MODE_MOMENT_CONTROL:
            self.get_axis(axis_num).controller.input_moment = target
        elif self.control_mode == CONTROL_MODE_POSITION_CONTROL:
            self.get_axis(axis_num).controller.input_pos = target
        else:
            raise Exception(f"Unsupported control mode: {self.control_mode}")

    def get_state(self, axis_num: int):
        """Get the state of a certain axis.

        :param axis_num Axis to get the state of
        """
        return self.get_axis(axis_num).current_state

    def set_state(self, axis_num: int, state: int):
        """Set the state of an axis.

        :param axis_num Axis to set the state of
        :param state State to set the axis to
        """
        self.get_axis(axis_num).requested_state = state
        self.wait()

    def reset(self):
        """Reset all errors and set all input setpoints to 0"""
        for axis in self.axes:
            self.set_actuation_target(axis, 0)
        self.odrive_entity.clear_errors()
        self.wait()
        print("Resetted the ODrive.")

    def enumerate_axes(self):
        """Return an enumerator over all ODrive axes."""
        return zip(self.axes, [self.get_axis(axis) for axis in self.axes])
    
    def test_encoder(self):
        for axis in self.axes:
            check_encoder(self.get_axis(axis))
            print(f"Successfully checked encoder on axis {axis}")

    def test_function(self):
        for axis in self.axes:
            min_pos_rad, max_pos_rad = IU_to_rads(self.get_axis(axis))
            print("Test function successfully called")
            move_through_range(self.get_axis(axis))