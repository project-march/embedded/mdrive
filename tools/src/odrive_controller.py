import time
from typing import Optional, List
import math

from src.joint import Joint
from src.odrive_class import ODrive


class ODriveController:
    def __init__(self, odrive_entity, joints: List[Joint], config: dict, args):
        super().__init__()
        self.odrive = ODrive(odrive_entity, joints, config, args)
        self.config = config
        self.args = args

    def start(self):
        self.odrive.reset()
        if self.args.erase:
            self.odrive.erase_configuration()
        if self.args.set_idle:
            self.odrive.set_idle()
        if self.args.configure:
            self.odrive.apply_config()
        if self.args.set_pre_calibrated:
            self.odrive.set_pre_calibrated()
        if self.args.prepare:
            self.odrive.prepare()
        if self.args.actuate:
            self.perform_actuation_test()
        if self.args.perform_autotuning:
            self.perform_autotuning()

    def perform_autotuning(self):
        print("Preparing autotuning on the MDrive.")
        self.odrive.prepare()
        self.odrive.test_encoder()
        self.odrive.test_function()


    def perform_actuation_test(self):
        """Perform an actuation test that follow a cosinus.

        Specifically: f(t) = cos(((2 * pi) / period) * t) * amplitude

        Amplitude and period are retrieved from the actuation config.
        """
        try:
            start_time = time.time()
            while True:
                self.odrive.check_active()
                debug_msg = "Debug:\n"
                for axis in self.odrive.axes:
                    target = self.get_target(axis, time.time() - start_time)
                    debug_msg += f"\t- axis target {axis}: {target}\n"
                    debug_msg += f"\t- abs pos {axis}: {self.odrive.get_axis(axis).encoder.pos_abs}\n"
                    self.odrive.set_actuation_target(axis, target)
                print(debug_msg)
                time.sleep(self.get_actuation_param("cycle_time"))
        except KeyboardInterrupt:
            self.odrive.reset()

    def get_target(self, axis: int, time_from_start: float):
        return self.get_actuation_param("start_direction", axis) * math.cos(
            (math.tau / self.get_actuation_param("period", axis)) * time_from_start
        ) * self.get_actuation_param("amplitude", axis) + self.get_actuation_param(
            "offset", axis
        )

    def get_actuation_param(self, name: str, axis: Optional[int] = None):
        actuation_param = self.config["actuation"][name]
        if isinstance(actuation_param, dict):
            if axis is not None:
                return actuation_param[axis]
            raise Exception(f"Cannot determine value of {name}")
        return actuation_param
