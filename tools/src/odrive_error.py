from typing import Optional

from odrive.utils import dump_errors

from src.mock_odrive import MockODrive


class ODriveError(Exception):
    def __init__(self, odrive_entity, msg: Optional[str] = None):
        self.odrive_error_string = ""

        if msg is not None:
            self.odrive_error_string += msg + "\n"

        if not isinstance(odrive_entity, MockODrive):
            dump_errors(odrive_entity)
        super().__init__("ODrive has an error:\n" + self.odrive_error_string)

    def add_error_to_string(self, s: str):
        self.odrive_error_string += s + "\n"
