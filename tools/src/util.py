import os
from datetime import datetime
from typing import List

import yaml

from src.joint import Joint

ODRIVE_FIND_TIMEOUT = 10


def get_time():
    return datetime.now().strftime("%H:%M:%S.%f")[:-3]


def make_config(config_folder: str, args) -> dict:
    config = {
        "general": load_yaml(config_folder, "general"),
        "joints": load_yaml(config_folder, "joints"),
    }
    if args.configure:
        for config_type in ["rotational", "linear"]:
            config[config_type] = load_yaml(config_folder, config_type)
    if args.actuate:
        config["actuation"] = load_yaml(config_folder, "actuation")
    return config


def make_joints(joints: List[str], config: dict):
    return [Joint.make(joint_name, config) for joint_name in joints]


def load_yaml(config_folder, config_type):
    return yaml.load(
        open(os.path.join(config_folder, config_type) + ".yaml").read(),
        Loader=yaml.FullLoader,
    )
